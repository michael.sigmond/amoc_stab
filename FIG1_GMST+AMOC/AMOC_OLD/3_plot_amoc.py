"""
==========================
MS, 20190404

- Get 2D msftmz (AMOC) data, Calculate maximum of annual mean stream function with CDO

- Result: while numerical values are slightly different than in RTD, year-to-year variations are indistinguishable
- Result: reproduced plot that was made with 2_calc+plot_amoc_old.py (which calculated AMOC strength not with CDO but in python)
- 2_calc+plot_amoc_old2.py: changed order of calculation (maximum of monthly fields, then annual mean); this made a difference
                              and brought the results out of line of that in the RTD


"""
import os
import cmipdata as cd
import numpy as np
import matplotlib.pyplot as plt # for basic plotting
import nc as nc
import rms_plots as rpl
import rms_utils as rut
from scipy import stats
import matplotlib as mpl
from matplotlib.ticker import MultipleLocator
################settings
mpl.style.reload_library()
plt.style.use('rms_paper')
######################################################
# 1. Read DATA
######################################################

##50-member ensembles up to 2100
os.chdir('DATA_AMOC_SHORT')
ens_his_amoc1=cd.mkensemble('amoc_nh_am_*_historical-r*',prefix='amoc_nh_am_')
ens_15c_amoc1=cd.mkensemble('amoc_nh_am_*_lowwarming15*',prefix='amoc_nh_am_')
ens_20c_amoc1=cd.mkensemble('amoc_nh_am_*_lowwarming20*',prefix='amoc_nh_am_')
ens_30c_amoc1=cd.mkensemble('amoc_nh_am_*_lowwarming30*',prefix='amoc_nh_am_')

datadict_his = cd.loadfiles(ens_his_amoc1,'msftmyz')
datadict_15c1 = cd.loadfiles(ens_15c_amoc1,'msftmyz')
datadict_20c1 = cd.loadfiles(ens_20c_amoc1,'msftmyz')
datadict_30c1 = cd.loadfiles(ens_30c_amoc1,'msftmyza')

amoc_his=datadict_his['data']
amoc_15c1=datadict_15c1['data']
amoc_20c1=datadict_20c1['data']
amoc_30c1=datadict_30c1['data']

os.chdir('../')

##5-member ensembles after to 2100
os.chdir('DATA_AMOC_LONG')
ens_15c_amoc2=cd.mkensemble('amoc_nh_am_*_lowwarming15*',prefix='amoc_nh_am_')
ens_20c_amoc2=cd.mkensemble('amoc_nh_am_*_lowwarming20*',prefix='amoc_nh_am_')
ens_30c_amoc2=cd.mkensemble('amoc_nh_am_*_lowwarming30*',prefix='amoc_nh_am_')

datadict_15c2 = cd.loadfiles(ens_15c_amoc2,'msftmyz')
datadict_20c2 = cd.loadfiles(ens_20c_amoc2,'msftmyz')
datadict_30c2 = cd.loadfiles(ens_30c_amoc2,'msftmyza')

####### years    
years_his=np.arange(1950,2100+1)
years_his_pre30c=np.arange(1950,2061)
amoc_his_pre30c=amoc_his[:,0:rut.find_nearest(years_his,2061)]
years_15c1=np.arange(2021,2100+1)
years_20c1=np.arange(2036,2100+1)
years_30c1=np.arange(2061,2100+1)
years2=np.arange(2101,2600+1)

amoc_15c2=datadict_15c2['data'][:,-500::]
amoc_20c2=datadict_20c2['data'][:,-500::]
amoc_30c2=datadict_30c2['data'][:,-500::]

os.chdir('../')

######################################################
# 2. Plot 
######################################################


fig1, axs = plt.subplots(1,2, figsize=(8,8)); 
fig1.subplots_adjust(bottom=0.6,wspace=0)

#1950-2100############################################
ax=axs[0]
ax.set_xlim([1950, 2100]) 
    
ax.set_xticks(np.arange(1950,2125,25))
ax.xaxis.set_major_locator(MultipleLocator(50))
ax.xaxis.set_minor_locator(MultipleLocator(25)) 

ax.set_ylim([12, 19.2])
ax.yaxis.set_major_locator(MultipleLocator(2))
ax.yaxis.set_minor_locator(MultipleLocator(0.5))
ax.set_ylabel('Sv')
    
####### plot
##his##

kwargs={'linewidth': 1, 'color': 'gray'}
ax.plot(years_his_pre30c,np.mean(amoc_his_pre30c,axis=0),**kwargs)  
#kwargs={'color':'gray', 'alpha': 0.25, 'edgecolor':'none'}
#ax.fill_between(years_his,np.min(amoc_his,axis=0), np.max(amoc_his,axis=0),**kwargs)
                                
##15c##
kwargs={'linewidth': 1, 'color': 'deepskyblue'}
ax.plot(years_15c1,np.mean(amoc_15c1,axis=0),**kwargs)
#kwargs={'color':'deepskyblue', 'alpha': 0.25, 'edgecolor':'none'}
#ax.fill_between(years_15c1,np.min(amoc_15c1,axis=0), np.max(amoc_15c1,axis=0),**kwargs)

#ax.axhline(xmin=72./150.,y=np.mean(amoc_15c1,axis=0)[0],color="deepskyblue",linestyle='--',linewidth=0.5)

##20c##
kwargs={'linewidth': 1, 'color': 'orange'}
ax.plot(years_20c1,np.mean(amoc_20c1,axis=0),**kwargs)
#ax.axhline(xmin=87./150.,y=np.mean(amoc_20c1,axis=0)[0],color="orange",linestyle='--',linewidth=0.5)

##30c##
kwargs={'linewidth': 1, 'color': 'red'}
ax.plot(years_30c1,np.mean(amoc_30c1,axis=0),**kwargs)
#ax.axhline(xmin=112./150.,y=np.mean(amoc_30c1,axis=0)[0],color="red",linestyle='--',linewidth=0.5)


##legend##
ax.text(1955,14,'HIST+RCP8.5',color= 'gray')
ax.text(1955,13.5,'1.5$^\circ$C',color= 'deepskyblue')
ax.text(1955,13,'2.0$^\circ$C',color= 'orange')
ax.text(1955,12.5,'3.0$^\circ$C',color= 'red')


#2100-2600############################################
ax=axs[1]
ax.set_xlim([2100, 2600]) 
ax.set_xticks=np.arange(2200,2700,100) 
majorLocator = MultipleLocator(100)
minorLocator = MultipleLocator(25)
ax.xaxis.set_major_locator(majorLocator)
ax.xaxis.set_minor_locator(minorLocator) 

ax.set_ylim([12, 19.2])
#majint=2; minint=0.5
#majorLocator = MultipleLocator(majint)
#minorLocator = MultipleLocator(minint)
#ax.yaxis.set_major_locator(majorLocator)
#ax.yaxis.set_minor_locator(minorLocator)
ax.set_yticklabels((''))
ax.yaxis.set_tick_params(length=0,width=0)
####### plot                                
##15c##
kwargs={'linewidth': 0.5, 'color': 'deepskyblue'}
ax.plot(years2,np.mean(amoc_15c2,axis=0),**kwargs)
##20c##
kwargs={'linewidth': 0.5, 'color': 'orange'}
ax.plot(years2,np.mean(amoc_20c2,axis=0),**kwargs)
##30c##
kwargs={'linewidth': 0.5, 'color': 'red'}
ax.plot(years2,np.mean(amoc_30c2,axis=0),**kwargs)

#title and save############################################
fig1.suptitle('Atlantic Meridional Overturning Circulation',y=0.92)
ax.text(2080,10.5,'Year')
#rpl.add_title(ax,'AMOC (50 runs, ensemble mean)')
rpl.mysavefig(fig1,'fig3_AMOC.png')







