"""
==========================
- Reviewd by MS, 20190404

- Get 2D msftmz (AMOC) data, and calculate Maximum field of that field in python
- Using functions that quickly allow for calculation and plotting of other timeseries
- Only for 15c and 20c 
- Result: while numerical values are slightly different than in RTD, year-to-year variations are indistinguishable

"""
import os
import cmipdata as cd
import numpy as np
import matplotlib.pyplot as plt # for basic plotting
import nc as nc
import rms_plots as rpl
import rms_utils as rut
from scipy import stats
################settings
calc_tas_pi=False

######################################################
# 1a. Calculate GM annual mean TAS timeseries LE
######################################################
def calc_read_varnm(varnm,do_calc):

    if varnm=='msftmyz':
        my_cdo_str='cdo -yearmean {infile} {outfile}' 
        prefix='am_'

    #srcdir_base='/HOME/rms/DATA/CCC/'        
    srcdir_base='/raid/ra40/data/rms/DATA/CCC/'        

    if do_calc:
        print 'Calculating netcdf files for ' + varnm
        os.system('mkdir -p DATA_' + varnm)
        os.chdir('DATA_' + varnm)
        os.system('/bin/rm *.nc')

        # all historical 
        os.system('ln -s ' + srcdir_base +'/CanSISE/historical-r1/mon/'+varnm + '/*r[1-5]i1*.nc .')
        os.system('ln -s ' + srcdir_base +'/LOWWARMING/lowwarming15/mon/'+varnm + '/*r[1-5]i1*_2???01-2???12.nc .')
        os.system('ln -s ' + srcdir_base +'/LOWWARMING/lowwarming20/mon/'+varnm + '/*r[1-5]i1*_2???01-2???12.nc .')
        #os.system('ln -s ' + srcdir_base +'/CanSISE/historical-r1/mon/'+varnm + '/*r1i1*.nc .')
        #os.system('ln -s ' + srcdir_base +'/LOWWARMING/lowwarming15/mon/'+varnm + '/*r1i1*_2???01-2???12.nc .')
        #os.system('ln -s ' + srcdir_base +'/LOWWARMING/lowwarming20/mon/'+varnm + '/*r1i1*_2???01-2???12.nc .')

        # concat
        ens_his = cd.mkensemble('*historical-r*')
        ens_his = cd.cat_exp_slices(ens_his)
        ens_15c = cd.mkensemble('*lowwarming15*')
        ens_15c = cd.cat_exp_slices(ens_15c)
        ens_20c = cd.mkensemble('*lowwarming20*')
        ens_20c = cd.cat_exp_slices(ens_20c)
 
        # global and annual means            
        ens_his=cd.my_operator(ens_his, my_cdo_str, output_prefix=prefix,delete=True)
        ens_15c=cd.my_operator(ens_15c, my_cdo_str, output_prefix=prefix,delete=True)
        ens_20c=cd.my_operator(ens_20c, my_cdo_str, output_prefix=prefix,delete=True)
    
    else:
        os.chdir('DATA_' + varnm)
        ens_his=cd.mkensemble(prefix+varnm+'_*_historical-r*',prefix=prefix)
        ens_15c=cd.mkensemble(prefix+varnm+'_*_lowwarming15*',prefix=prefix)
        ens_20c=cd.mkensemble(prefix+varnm+'_*_lowwarming20*',prefix=prefix)
    
    return ens_his,ens_15c, ens_20c 

##calc and get ensembles
ens_his,ens_15c, ens_20c= calc_read_varnm('msftmyz',do_calc=False)
###########read
##
lat=nc.getvar((ens_his.lister('ncfile'))[0],'lat'); nlat=np.size(lat)
lev=nc.getvar((ens_his.lister('ncfile'))[0],'lev'); nlev=np.size(lev)

n_his=np.size(ens_his.lister('ncfile'))
n_15c=np.size(ens_15c.lister('ncfile'))
n_20c=np.size(ens_20c.lister('ncfile'))

years_his=nc.getvar((ens_his.lister('ncfile'))[0],'time');nt_his=np.size(years_his)
years_15c=nc.getvar((ens_15c.lister('ncfile'))[0],'time');nt_15c=np.size(years_15c)
years_20c=nc.getvar((ens_20c.lister('ncfile'))[0],'time');nt_20c=np.size(years_20c)

var_his=np.ma.zeros((n_his,nt_his,nlev,nlat))
var_15c=np.ma.zeros((n_15c,nt_15c,nlev,nlat))
var_20c=np.ma.zeros((n_20c,nt_20c,nlev,nlat))

for i,ifile in enumerate(sorted(ens_his.lister('ncfile'))):
  var_his[i,:,:,:]=np.ma.masked_greater(nc.getvar(ifile,'msftmyz')[:,0,:,:].squeeze(),9e19) 

for i,ifile in enumerate(sorted(ens_15c.lister('ncfile'))):
  var_15c[i,:,:,:]=np.ma.masked_greater(nc.getvar(ifile,'msftmyz')[:,0,:,:].squeeze(),9e19) 

for i,ifile in enumerate(sorted(ens_20c.lister('ncfile'))):
  var_20c[i,:,:,:]=np.ma.masked_greater(nc.getvar(ifile,'msftmyz')[:,0,:,:].squeeze(),9e19) 

os.chdir('../')
######################################################
# 1c. Calculate maximum of North Atlantic
######################################################
varmax_his=np.zeros((n_his,nt_his))
varmax_15c=np.zeros((n_15c,nt_15c))
varmax_20c=np.zeros((n_20c,nt_20c))

for i in range(n_his):
  for it in range(nt_his):
      varmax_his[i,it]=np.max(var_his[i,it,:,rut.find_nearest(lat,20)::])
      #varmax_his[i,it]=np.max(var_his[i,it,:,:])

for i in range(n_15c):
  for it in range(nt_15c):
      varmax_15c[i,it]=np.max(var_15c[i,it,:,rut.find_nearest(lat,20)::])
      #varmax_15c[i,it]=np.max(var_15c[i,it,:,:])

for i in range(n_20c):
  for it in range(nt_20c):
      varmax_20c[i,it]=np.max(var_20c[i,it,:,rut.find_nearest(lat,20)::])
      #varmax_20c[i,it]=np.max(var_20c[i,it,:,:])




######################################################
# 2. Plot
######################################################
######################################################
# 2a. test plot 2D
######################################################

fig1, axs = plt.subplots(1,1, figsize=(8,8)); fig1.subplots_adjust(bottom=0.2,hspace=0.3,wspace=0.25)
xs,ys=np.meshgrid(lat,lev)  
axs.invert_yaxis()
cf=axs.contourf(xs,ys,var_15c[0,0,:,:].squeeze()/1e6)




def plot_ts(ax,var_his,var_15c,var_20c,varname,units):

    ####### years    
    years_his=np.arange(1950,2100+1)
    years_15c=np.arange(2021,2600+1)
    years_20c=np.arange(2036,2600+1)

    ####### stats    
    var_his_mean=np.mean(var_his,axis=0)
    #var_his_std=np.std(var_his,axis=0)
    
    var_15c_mean=np.mean(var_15c,axis=0)
    #var_15c_std=np.std(var_15c,axis=0)

    var_20c_mean=np.mean(var_20c,axis=0)
    #var_20c_std=np.std(var_20c,axis=0)

    #nens_his=np.shape(var_his)[0]
    #nens_15c=np.shape(var_15c)[0]
    #nens_20c=np.shape(var_20c)[0]
    
    #t_his = stats.t.isf(0.025,nens_his-1) 
    #t_15c = stats.t.isf(0.025,nens_15c-1) 
    #t_20c = stats.t.isf(0.025,nens_20c-1) 
    
    #var_his_ci95 = (t_his * var_his_std)/np.sqrt(nens_his) 
    #var_15c_ci95 = (t_15c * var_15c_std)/np.sqrt(nens_15c) 
    #var_20c_ci95 = (t_20c * var_20c_std)/np.sqrt(nens_20c) 
    
    # Axis 
    #ax.set_xlim([1950, 2050]) 
    ax.set_xlim([1950, 2300]) 
    
    #ax.set_xticks(np.arange(1950,2650,100))
    ax.set_ylim([13, 19.2])

    ax.set_ylabel(units)

    # Lines        
    #ax.axhline(y=0.0,color="black",linestyle='-',linewidth=1)
    #ax.axhline(y=np.mean(var_15c_mean),color="black",linestyle='--',linewidth=1)
    #ax.axhline(y=np.mean(var_20c_mean),color="black",linestyle='--',linewidth=1)
    
    #if varname=='sic': ax.axhline(y=1.0,color="black",linestyle='-',linewidth=1)
    #print np.mean(var_15c_mean)
    #print np.mean(var_20c_mean)
    
    ####### plot
    ##his##
    kwargs={'linewidth': 1, 'color': 'gray'}
    ax.plot(years_his,var_his_mean,**kwargs)
    #ci95
    #ax.fill_between(years_his, var_his_mean - var_his_ci95,var_his_mean + var_his_ci95 ,
    #               color='red', alpha=0.25, edgecolor='none')
    #range
    #ax.fill_between(years_his,np.min(var_his,axis=0), np.max(var_his,axis=0),
    #                                color='red', alpha=0.15, edgecolor='none')
                                     
    ##15c##
    kwargs={'linewidth': 1, 'color': 'deepskyblue'}
    ax.plot(years_15c,var_15c_mean,**kwargs)
    #ci95
    #ax.fill_between(years_15c, var_15c_mean - var_15c_ci95,var_15c_mean + var_15c_ci95,
    #               color='k', alpha=0.25, edgecolor='none')
    #range
    #ax.fill_between(years_15c,np.min(var_15c,axis=0), np.max(var_15c,axis=0),
    #                                color='green', alpha=0.15, edgecolor='none')
    ##connection his-to-15c
    #kwargs={'linewidth':0.5 , 'color': 'green'}
    #ax.plot(np.arange(2020,2022),np.array([var_his_mean[70],var_15c_mean[0]]),**kwargs)

    ##20c##
    kwargs={'linewidth': 1, 'color': 'orange'}
    ax.plot(years_20c,var_20c_mean,**kwargs)
    #ci95
    #ax.fill_between(years_20c, var_20c_mean - var_20c_ci95,var_20c_mean + var_20c_ci95,
    #               color='k', alpha=0.25, edgecolor='none')
    #range
    #ax.fill_between(years_20c,np.min(var_20c,axis=0), np.max(var_20c,axis=0),
    #                                color='orange', alpha=0.15, edgecolor='none')
    ##connection his-to-20c
    #kwargs={'linewidth':0.5 , 'color': 'orange'}
    #ax.plot(np.arange(2035,2037),np.array([var_his_mean[85],var_20c_mean[0]]),**kwargs)

    ax.text(1955,18.8,'RCP8.5',color= 'gray')
    ax.text(2025,18.8,'2.0$^\circ$C',color= 'orange')
    ax.text(2095,18.8,'1.5$^\circ$C',color= 'deepskyblue')

# plots=====================================
# FIG1
fig1, axs = plt.subplots(1,1, figsize=(8,8)); fig1.subplots_adjust(bottom=0.6,right=0.5,hspace=0.3,wspace=0.25)
#
plot_ts(axs,varmax_his/1.025e9, varmax_15c/1.025e9,varmax_20c/1.025e9,'','AMOC(Sv)')
rpl.add_title(axs,'')
#axs[0,1].axis('off')
#axs[1,0].axis('off')
#axs[1,1].axis('off')

#plot_ts(axs[0,1],pr_his,pr_15c,pr_20c,'pr',units='mm/day')
#rpl.add_title(axs[0,1],'Precipitation','b')
#
#plot_ts(axs[1,0],sic_his,sic_15c,sic_20c,'sic','10$^6$ km$^2$')
#rpl.add_title(axs[1,0],'September Sea Ice Extent','c')
#
#plot_ts(axs[1,1],snw_his,snw_15c,snw_20c,'snw','mm')
#rpl.add_title(axs[1,1],'Snow Water Equivalent','d')
#
#plot_ts(axs[2,0],zostoga_his,zostoga_15c,zostoga_20c,'zostoga','cm')
#rpl.add_title(axs[2,0],'Thermosteric sea level','e')
#
#
rpl.mysavefig(fig1,'fig2_AMOC_old.png')














