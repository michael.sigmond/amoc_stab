"""
MS, 20190404

- 2_calc+plot_amoc_old2.py: changed order of calculation (annual mean of the maximum of monthly fields); this made a difference
                              and brought the results out of line of that in the RTD



"""
import os
import cmipdata as cd
import numpy as np
import matplotlib.pyplot as plt # for basic plotting
import nc as nc
import rms_plots as rpl
import rms_utils as rut
from scipy import stats
################settings
calc_tas_pi=False

######################################################
# 1a. Calculate GM annual mean TAS timeseries LE
######################################################
#srcdir_base='/HOME/rms/DATA/CCC/'        
srcdir_base='/raid/ra40/data/rms/DATA/CCC/'        
do_calc=True

if do_calc:
    print 'Calculating netcdf files for AMOC'
    os.system('mkdir -p DATA_AMOC')
    os.chdir('DATA_AMOC')
    os.system('/bin/rm *.nc')

    # all historical 
    os.system('ln -s ' + srcdir_base +'/LOWWARMING/lowwarming15/mon/msftmyz/*r1i1*_2???01-2???12.nc .')
    # concat
    ens_15c = cd.mkensemble('*lowwarming15*')
    ens_15c = cd.cat_exp_slices(ens_15c)

    # 20-90N            
    my_ncks_str='ncks -d basin,0 -d lat,117,191 {infile} {outfile}' ; prefix='nh_'
    ens_15c_nh=cd.my_operator(ens_15c, my_ncks_str, output_prefix='nh_',delete=False)

    # Annual mean of the Maximum of monthly fields, convert from mass (kg/s to volume [Sv])            
    my_cdo_str='cdo -divc,1.025e9 -yearmean -vertmax -fldmax {infile} {outfile}' ; prefix='amoc_'
    ens_15c_amoc=cd.my_operator(ens_15c_nh, my_cdo_str, output_prefix=prefix,delete=True)
 
else:
    os.chdir('DATA_AMOC')
    ens_15c_amoc=cd.mkensemble('amoc_nh_am_*_lowwarming15*',prefix='amoc_nh_am_')

datadict_15c = cd.loadfiles(ens_15c_amoc,'msftmyz')


amoc_15c=datadict_15c['data']

os.chdir('../')


######################################################
# 2. Plot
######################################################
fig1, ax = plt.subplots(1,1, figsize=(8,8)); 
fig1.subplots_adjust(bottom=0.6,right=0.5,hspace=0.3,wspace=0.25)

####### years    
years_15c=np.arange(2021,2600+1)

####### stats    
amoc_15c_mean=np.mean(amoc_15c,axis=0)
# Axis 
#ax.set_xlim([1950, 2050]) 
#ax.set_xlim([1950, 2300]) 
ax.set_xlim([2021, 2120]) 
    
#ax.set_xticks(np.arange(1950,2650,100))
ax.set_ylim([12, 19.2])
ax.set_ylabel('Sv')
    
####### plot
##his##
kwargs={'linewidth': 1, 'color': 'gray'}
#ax.plot(years_his,var_his_mean,**kwargs)                                    
##15c##
kwargs={'linewidth': 1, 'color': 'deepskyblue'}
#ax.plot(years_15c,amoc_15c_mean,**kwargs)
ax.plot(years_15c,amoc_15c[0,:],**kwargs)
##20c##
kwargs={'linewidth': 1, 'color': 'orange'}
#ax.plot(years_20c,amoc_20c_mean,**kwargs)
##30c##
kwargs={'linewidth': 1, 'color': 'red'}

##legend##
#ax.text(1955,18.8,'RCP8.5',color= 'gray')
#ax.text(2025,18.8,'2.0$^\circ$C',color= 'orange')
#ax.text(2095,18.8,'1.5$^\circ$C',color= 'deepskyblue')

# plots=====================================
#
rpl.mysavefig(fig1,'fig2_AMOC_old2.png')














