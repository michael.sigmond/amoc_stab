"""
==========================
MS, 20190404

- Get 2D msftmz (AMOC) data, Calculate maximum of annual mean stream function with CDO

- Result: while numerical values are slightly different than in RTD, year-to-year variations are indistinguishable
- Result: reproduced plot that was made with 2_calc+plot_amoc_old.py (which calculated AMOC strength not with CDO but in python)
- 2_calc+plot_amoc_old2.py: changed order of calculation (maximum of monthly fields, then annual mean); this made a difference
                              and brought the results out of line of that in the RTD


"""
import os
import cmipdata as cd
import numpy as np
import matplotlib.pyplot as plt # for basic plotting
import nc as nc
import rms_plots as rpl
import rms_utils as rut
from scipy import stats
################settings
calc_tas_pi=False

######################################################
# 1a. Calculate GM annual mean TAS timeseries LE
######################################################
#srcdir_base='/HOME/rms/DATA/CCC/'        
srcdir_base='/raid/ra40/data/rms/DATA/CCC/'        
do_calc=False

if do_calc:
    os.system('mkdir -p DATA_AMOC_LONG')
    os.chdir('DATA_AMOC_LONG')
    os.system('/bin/rm *.nc')

    # all historical 
    os.system('ln -s ' + srcdir_base +'/CanSISE/historical-r*/mon/msftmyz/*r*i1*.nc .')
    os.system('ln -s ' + srcdir_base +'/LOWWARMING/lowwarming15/mon/msftmyz/*r[1-5]i1*_2???01-2???12.nc .')
    os.system('ln -s ' + srcdir_base +'/LOWWARMING/lowwarming20/mon/msftmyz/*r[1-5]i1*_2???01-2???12.nc .')
    os.system('ln -s ' + srcdir_base +'/LOWWARMING/lowwarming30/mon/msftmyza/*r[1-5]i1*_2???01-2???12.nc .')

    # concat
    ens_his = cd.mkensemble('*historical-r*')
    ens_his = cd.cat_exp_slices(ens_his)
    ens_15c = cd.mkensemble('*lowwarming15*')
    ens_15c = cd.cat_exp_slices(ens_15c)
    ens_20c = cd.mkensemble('*lowwarming20*')
    ens_20c = cd.cat_exp_slices(ens_20c)
    ens_30c = cd.mkensemble('*lowwarming30*')
    ens_30c = cd.cat_exp_slices(ens_30c)

    print 'Calculating annual mean psi'
    # annual means            
    my_cdo_str='cdo -yearmean {infile} {outfile}' 
    ens_his_2d=cd.my_operator(ens_his, my_cdo_str, output_prefix='am_',delete=True)
    ens_15c_2d=cd.my_operator(ens_15c, my_cdo_str, output_prefix='am_',delete=True)
    ens_20c_2d=cd.my_operator(ens_20c, my_cdo_str, output_prefix='am_',delete=True)
    ens_30c_2d=cd.my_operator(ens_30c, my_cdo_str, output_prefix='am_',delete=True)

    print 'Select NH'
    # 20-90N            
    my_ncks_str='ncks -d basin,0 -d lat,117,191 {infile} {outfile}' ; prefix='nh_'
    ens_his_nh=cd.my_operator(ens_his_2d, my_ncks_str, output_prefix='nh_',delete=True)
    ens_15c_nh=cd.my_operator(ens_15c_2d, my_ncks_str, output_prefix='nh_',delete=True)
    ens_20c_nh=cd.my_operator(ens_20c_2d, my_ncks_str, output_prefix='nh_',delete=True)
    my_ncks_str='ncks -d lat,117,191 {infile} {outfile}' ; prefix='nh_'
    ens_30c_nh=cd.my_operator(ens_30c_2d, my_ncks_str, output_prefix='nh_',delete=True)

    print 'Calculate AMOC'
    # Max, and convert from mass (kg/s to volume [Sv])            
    my_cdo_str='cdo -divc,1.025e9 -vertmax -fldmax {infile} {outfile}' ; prefix='amoc_'
    ens_his_amoc=cd.my_operator(ens_his_nh, my_cdo_str, output_prefix=prefix,delete=True)
    ens_15c_amoc=cd.my_operator(ens_15c_nh, my_cdo_str, output_prefix=prefix,delete=True)
    ens_20c_amoc=cd.my_operator(ens_20c_nh, my_cdo_str, output_prefix=prefix,delete=True)
    ens_30c_amoc=cd.my_operator(ens_30c_nh, my_cdo_str, output_prefix=prefix,delete=True)
 
else:
    os.chdir('DATA_AMOC_LONG')
    ens_his_amoc=cd.mkensemble('amoc_nh_am_*_historical-r*',prefix='amoc_nh_am_')
    ens_15c_amoc=cd.mkensemble('amoc_nh_am_*_lowwarming15*',prefix='amoc_nh_am_')
    ens_20c_amoc=cd.mkensemble('amoc_nh_am_*_lowwarming20*',prefix='amoc_nh_am_')
    ens_30c_amoc=cd.mkensemble('amoc_nh_am_*_lowwarming30*',prefix='amoc_nh_am_')

datadict_his = cd.loadfiles(ens_his_amoc,'msftmyz')
datadict_15c = cd.loadfiles(ens_15c_amoc,'msftmyz')
datadict_20c = cd.loadfiles(ens_20c_amoc,'msftmyz')
datadict_30c = cd.loadfiles(ens_30c_amoc,'msftmyza')


amoc_his=datadict_his['data']
amoc_15c=datadict_15c['data']
amoc_20c=datadict_20c['data']
amoc_30c=datadict_30c['data']

os.chdir('../')


######################################################
# 2. Plot
######################################################
fig1, ax = plt.subplots(1,1, figsize=(8,8)); 
fig1.subplots_adjust(bottom=0.6,right=0.5,hspace=0.3,wspace=0.25)

####### years    
years_his=np.arange(1950,2100+1)
years_15c=np.arange(2021,2600+1)
years_20c=np.arange(2036,2600+1)
years_30c=np.arange(2061,2600+1)

####### stats    
amoc_his_mean=np.mean(amoc_his,axis=0)
amoc_15c_mean=np.mean(amoc_15c,axis=0)
amoc_20c_mean=np.mean(amoc_20c,axis=0)
amoc_30c_mean=np.mean(amoc_30c,axis=0)
 
# Axis 
#ax.set_xlim([1950, 2050]) 
#ax.set_xlim([1950, 2300]) 
ax.set_xlim([1950, 2600]) 
    
ax.set_xticks(np.arange(1950,2650,100))
ax.set_ylim([12, 19.2])
ax.set_ylabel('Sv')
    
####### plot
meanstart=-202
##his##
kwargs={'linewidth': 0.5, 'color': 'gray'}
ax.plot(years_his,amoc_his_mean,**kwargs)                                    
##15c##
kwargs={'linewidth': 0.5, 'color': 'deepskyblue'}
ax.plot(years_15c,amoc_15c_mean,**kwargs)
ax.axhline(y=np.mean(amoc_15c_mean[meanstart:-1]),color="deepskyblue",linestyle='--',linewidth=0.5)
ax.scatter([2610,2610,2610,2610,2610],np.mean(amoc_15c[:,meanstart:-1],axis=1),clip_on=False,facecolors='none', edgecolors='deepskyblue',s=2)



##20c##
kwargs={'linewidth': 0.5, 'color': 'orange'}
ax.plot(years_20c,amoc_20c_mean,**kwargs)
ax.axhline(y=np.mean(amoc_20c_mean[meanstart:-1]),color="orange",linestyle='--',linewidth=0.5)
ax.scatter([2620,2620,2620,2620,2620],np.mean(amoc_20c[:,meanstart:-1],axis=1),clip_on=False,facecolors='none', edgecolors='orange',s=2)

##30c##
kwargs={'linewidth': 0.5, 'color': 'red'}
ax.plot(years_30c,amoc_30c_mean,**kwargs)
ax.axhline(y=np.mean(amoc_30c_mean[meanstart:-1]),color="red",linestyle='--',linewidth=0.5)
ax.scatter([2630,2630,2630,2630,2630],np.mean(amoc_30c[:,meanstart:-1],axis=1),clip_on=False,facecolors='none', edgecolors='red',s=2)


##legend##
##legend##
ax.text(2350,14.5,'RCP8.5 (50 runs)',color= 'gray')
ax.text(2350,14,'1.5$^\circ$C (5 runs)',color= 'deepskyblue')
ax.text(2350,13.5,'2.0$^\circ$C (5 runs)',color= 'orange')
ax.text(2350,13,'3.0$^\circ$C (5 runs)',color= 'red')

# plots=====================================
#
rpl.add_title(ax,'AMOC (5 runs)')
rpl.mysavefig(fig1,'fig2_AMOC_long.png')














