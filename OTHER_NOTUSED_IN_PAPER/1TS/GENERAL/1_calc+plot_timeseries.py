"""
==========================

Get CMIP5 tas data, join the historical and RCP85 experiments (and time-slices within),
select a period of interest, then remap all models to a common 1x1 degree grid in space.

1. Import the tas data for historical and rcp85 experiments
2. Join all time-slices (within and across experiments)
3. Select the period of interest (1980-01 to 2013-12)
4. remap
5. Load the data (loading_tools) and make plots (plotting_tools)


"""
import os
import cmipdata as cd
import numpy as np
import matplotlib.pyplot as plt # for basic plotting

import rms_plots as rpl
from scipy import stats
################settings
calc_tas_pi=False

######################################################
# 1a. Calculate GM annual mean TAS timeseries LE
######################################################
def calc_read_var(varnm,do_calc):

    if varnm=='sic':
        my_cdo_str='cdo -mulc,255.0322 -fldmean -sellonlatbox,0,360,0,90 -gtc,15 -selmon,9 {infile} {outfile}' 
        prefix='ssie_'
    elif varnm=='zostoga':
        my_cdo_str='cdo -yearmean {infile} {outfile}' 
        prefix='am_'
    elif varnm=='snw':
        my_cdo_str='cdo -fldmean -yearmean {infile} {outfile}' 
        prefix='am_'
    else:   #tas,pr,rsds,rsdsc,clt
        my_cdo_str='cdo -fldmean -yearmean {infile} {outfile}' 
        prefix='amgm_'

    #srcdir_base='/HOME/rms/DATA/CCC/'        
    srcdir_base='/raid/ra40/data/rms/DATA/CCC/'        

    if do_calc:
        print 'Calculating netcdf files for ' + varnm
        os.system('mkdir -p DATA_' + varnm)
        os.chdir('DATA_' + varnm)
        os.system('/bin/rm *.nc')

        # all historical 
        os.system('ln -s ' + srcdir_base +'/CanSISE/historical-r1/mon/'+varnm + '/*r[1-5]i1*.nc .')
        os.system('ln -s ' + srcdir_base +'/LOWWARMING/lowwarming15/mon/'+varnm + '/*r[1-5]i1*_2???01-2???12.nc .')
        os.system('ln -s ' + srcdir_base +'/LOWWARMING/lowwarming20/mon/'+varnm + '/*r[1-5]i1*_2???01-2???12.nc .')

        # concat
        ens_his = cd.mkensemble('*historical-r*')
        ens_his = cd.cat_exp_slices(ens_his)
        ens_15c = cd.mkensemble('*lowwarming15*')
        ens_15c = cd.cat_exp_slices(ens_15c)
        ens_20c = cd.mkensemble('*lowwarming20*')
        ens_20c = cd.cat_exp_slices(ens_20c)
 
        # global and annual means            
        ens_his=cd.my_operator(ens_his, my_cdo_str, output_prefix=prefix,delete=True)
        ens_15c=cd.my_operator(ens_15c, my_cdo_str, output_prefix=prefix,delete=True)
        ens_20c=cd.my_operator(ens_20c, my_cdo_str, output_prefix=prefix,delete=True)
    
    else:
        os.chdir('DATA_' + varnm)
        ens_his=cd.mkensemble(prefix+varnm+'_*_historical-r*',prefix=prefix)
        ens_15c=cd.mkensemble(prefix+varnm+'_*_lowwarming15*',prefix=prefix)
        ens_20c=cd.mkensemble(prefix+varnm+'_*_lowwarming20*',prefix=prefix)

    datadict_his = cd.loadfiles(ens_his,varnm)
    datadict_15c = cd.loadfiles(ens_15c,varnm)
    datadict_20c = cd.loadfiles(ens_20c,varnm)

    var_his=datadict_his['data']
    var_15c=datadict_15c['data']
    var_20c=datadict_20c['data']

    os.chdir('../')
    
    return var_his,var_15c, var_20c 

##basic
tas_his,tas_15c,tas_20c = calc_read_var('tas',do_calc=False)
pr_his,pr_15c,pr_20c    = calc_read_var('pr',do_calc=False)
sic_his,sic_15c,sic_20c = calc_read_var('sic',do_calc=False)
zostoga_his,zostoga_15c,zostoga_20c=calc_read_var('zostoga',do_calc=False)
snw_his,snw_15c,snw_20c=calc_read_var('snw',do_calc=False)



######################################################
# 1b. Calculate GM annual mean PI TAS timeseries
######################################################
if calc_tas_pi:
    os.system('mkdir -p DATA_TAS_PI')
    os.chdir('DATA_TAS_PI')    
    os.system('ln -s /HOME/rms/DATA/CCC/CMIP5/piControl/mon/tas/tas_Amon_CanESM2_piControl_r1i1p1_*.nc .')
    # concat
    ens_tas_raw = cd.mkensemble('tas_Amon_CanESM2_piControl*')
    ens_tas = cd.cat_exp_slices(ens_tas_raw)
    # global and annual means
    my_cdo_str='cdo -fldmean -yearmean {infile} {outfile}' 
    ens_tas_pi=cd.my_operator(ens_tas, my_cdo_str, output_prefix='amgm_',delete=True)
else:
    os.chdir('DATA_TAS_PI')
    ens_tas_pi=cd.mkensemble('amgm_tas_Amon*',prefix='amgm_')

datadict_tas_pi = cd.loadfiles(ens_tas_pi,'tas')
tas_pi=datadict_tas_pi['data']
os.chdir('../')

######################################################
# 1c. Post
######################################################


tas_his=tas_his-np.mean(tas_pi)
tas_15c=tas_15c-np.mean(tas_pi)
tas_20c=tas_20c-np.mean(tas_pi)


pr_his=pr_his*86400
pr_15c=pr_15c*86400
pr_20c=pr_20c*86400

zostoga_his=zostoga_his*100
zostoga_15c=zostoga_15c*100
zostoga_20c=zostoga_20c*100


######################################################
# 2. Plot
######################################################

def plot_ts(ax,var_his,var_15c,var_20c,varname,units):

    ####### years    
    years_his=np.arange(1950,2100+1)
    years_15c=np.arange(2021,2600+1)
    years_20c=np.arange(2036,2600+1)

    ####### stats    
    var_his_mean=np.mean(var_his,axis=0)
    var_his_std=np.std(var_his,axis=0)
    
    var_15c_mean=np.mean(var_15c,axis=0)
    var_15c_std=np.std(var_15c,axis=0)

    var_20c_mean=np.mean(var_20c,axis=0)
    var_20c_std=np.std(var_20c,axis=0)

    nens_his=np.shape(var_his)[0]
    nens_15c=np.shape(var_15c)[0]
    nens_20c=np.shape(var_20c)[0]
    
    t_his = stats.t.isf(0.025,nens_his-1) 
    t_15c = stats.t.isf(0.025,nens_15c-1) 
    t_20c = stats.t.isf(0.025,nens_20c-1) 
    
    var_his_ci95 = (t_his * var_his_std)/np.sqrt(nens_his) 
    var_15c_ci95 = (t_15c * var_15c_std)/np.sqrt(nens_15c) 
    var_20c_ci95 = (t_20c * var_20c_std)/np.sqrt(nens_20c) 
    
    # Axis 
    ax.set_xlim([1950, 2600]) 
    ax.set_xticks(np.arange(1950,2650,100))
    if varname=='tas': ax.set_ylim([-0.5, 3.0])
    if varname=='pr':  ax.set_ylim([2.7, 2.88])
    if varname=='sic': ax.set_ylim([0, 7])
    if varname=='zostoga':ax.set_ylim([-5, 25])    
    if varname=='snw':ax.set_ylim([20, 25])    

    ax.set_ylabel(units)

    # Lines        
    ax.axhline(y=0.0,color="black",linestyle='-',linewidth=1)
    ax.axhline(y=np.mean(var_15c_mean),color="black",linestyle='--',linewidth=1)
    ax.axhline(y=np.mean(var_20c_mean),color="black",linestyle='--',linewidth=1)
    
    if varname=='sic': ax.axhline(y=1.0,color="black",linestyle='-',linewidth=1)
    print np.mean(var_15c_mean)
    print np.mean(var_20c_mean)
    
    ####### plot
    ##his##
    kwargs={'linewidth': 0.5, 'color': 'red'}
    ax.plot(years_his,var_his_mean,**kwargs)
    #ci95
    #ax.fill_between(years_his, var_his_mean - var_his_ci95,var_his_mean + var_his_ci95 ,
    #               color='red', alpha=0.25, edgecolor='none')
    #range
    ax.fill_between(years_his,np.min(var_his,axis=0), np.max(var_his,axis=0),
                                    color='red', alpha=0.15, edgecolor='none')
                                     
    ##15c##
    kwargs={'linewidth': 0.5, 'color': 'green'}
    ax.plot(years_15c,var_15c_mean,**kwargs)
    #ci95
    #ax.fill_between(years_15c, var_15c_mean - var_15c_ci95,var_15c_mean + var_15c_ci95,
    #               color='k', alpha=0.25, edgecolor='none')
    #range
    ax.fill_between(years_15c,np.min(var_15c,axis=0), np.max(var_15c,axis=0),
                                    color='green', alpha=0.15, edgecolor='none')
    ##connection his-to-15c
    kwargs={'linewidth':0.5 , 'color': 'green'}
    ax.plot(np.arange(2020,2022),np.array([var_his_mean[70],var_15c_mean[0]]),**kwargs)

    ##20c##
    kwargs={'linewidth': 0.5, 'color': 'orange'}
    ax.plot(years_20c,var_20c_mean,**kwargs)
    #ci95
    #ax.fill_between(years_20c, var_20c_mean - var_20c_ci95,var_20c_mean + var_20c_ci95,
    #               color='k', alpha=0.25, edgecolor='none')
    #range
    ax.fill_between(years_20c,np.min(var_20c,axis=0), np.max(var_20c,axis=0),
                                    color='orange', alpha=0.15, edgecolor='none')
    ##connection his-to-20c
    kwargs={'linewidth':0.5 , 'color': 'orange'}
    ax.plot(np.arange(2035,2037),np.array([var_his_mean[85],var_20c_mean[0]]),**kwargs)



# plots=====================================
# FIG1
fig1, axs = plt.subplots(3,2, figsize=(8,8)); fig1.subplots_adjust(bottom=0.2,hspace=0.3,wspace=0.25)

plot_ts(axs[0,0],tas_his,tas_15c,tas_20c,'tas','$^\circ$C')
rpl.add_title(axs[0,0],'Surface temperature','a')

plot_ts(axs[0,1],pr_his,pr_15c,pr_20c,'pr',units='mm/day')
rpl.add_title(axs[0,1],'Precipitation','b')

plot_ts(axs[1,0],sic_his,sic_15c,sic_20c,'sic','10$^6$ km$^2$')
rpl.add_title(axs[1,0],'September Sea Ice Extent','c')

plot_ts(axs[1,1],snw_his,snw_15c,snw_20c,'snw','mm')
rpl.add_title(axs[1,1],'Snow Water Equivalent','d')

plot_ts(axs[2,0],zostoga_his,zostoga_15c,zostoga_20c,'zostoga','cm')
rpl.add_title(axs[2,0],'Thermosteric sea level','e')


rpl.mysavefig(fig1,'fig1.png')














