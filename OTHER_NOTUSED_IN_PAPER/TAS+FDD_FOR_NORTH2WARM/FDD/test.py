# -*- coding: utf-8 -*-
"""
Created on Fri Oct 20 14:15:10 2017

@author: acrnrms
"""
import matplotlib.pyplot as plt 


a=[1.13,1.5,1.64,2,2.19]
b=[0.024,0.13,0.190,0.5,0.633]

fig, ax1 = plt.subplots(1,1, figsize=(8,8)); fig.subplots_adjust(right=0.6,bottom=0.5)
ax1.scatter(a,b)
