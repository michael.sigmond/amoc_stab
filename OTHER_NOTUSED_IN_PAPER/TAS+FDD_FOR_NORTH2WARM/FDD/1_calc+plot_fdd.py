"""
==========================

Get CMIP5 tas data, join the historical and RCP85 experiments (and time-slices within),
select a period of interest, then remap all models to a common 1x1 degree grid in space.

1. Import the tas data for historical and rcp85 experiments
2. Join all time-slices (within and across experiments)
3. Select the period of interest (1980-01 to 2013-12)
4. remap
5. Load the data (loading_tools) and make plots (plotting_tools)


"""
import os
import cmipdata as cd
import numpy as np
import matplotlib.pyplot as plt # for basic plotting
import nc as nc
import rms_plots as rpl
import rms_utils as rut
from scipy import stats
################settings
workdir=os.getcwd()
k_to_c=-273.16
################################################################
# 1a. Calculate GM annual mean TAS timeseries Stabilization LEs#
################################################################
def calc_read_ts(varnm,do_calc):

    srcdir_base='/HOME/rms/DATA/CCC/'        
    #srcdir_base='/raid/ra40/data/rms/DATA/CCC/'        
    os.system('mkdir -p DATA_' + varnm)
    os.chdir('DATA_' + varnm)

    if do_calc:
        os.system('/bin/rm *.nc')

        # all historical 
        os.system('ln -s ' + srcdir_base +'/LOWWARMING/lowwarming15/mon/'+varnm + '/*_2[0]??01-2[0,1]??12.nc .')
        os.system('ln -s ' + srcdir_base +'/LOWWARMING/lowwarming20/mon/'+varnm + '/*_2[0]??01-2[0,1]??12.nc .')
        # concat
        ens_15c = cd.mkensemble('*lowwarming15*')
        ens_15c = cd.cat_exp_slices(ens_15c)
        ens_20c = cd.mkensemble('*lowwarming20*')
        ens_20c = cd.cat_exp_slices(ens_20c)
        
        #annual mean
        my_cdo_str='cdo -seldate,2100-01-01,2100-12-31 {infile} {outfile}'
        ens_15c=cd.my_operator(ens_15c, my_cdo_str, output_prefix='ts_',delete=True)
        ens_20c=cd.my_operator(ens_20c, my_cdo_str, output_prefix='ts_',delete=True)       
    else:
        ens_15c=cd.mkensemble('ts_tas_Amon*lowwarming15*',prefix='ts_')
        ens_20c=cd.mkensemble('ts_tas_Amon*lowwarming20*',prefix='ts_')

    datadict_var_15c = cd.loadfiles(ens_15c,'tas')
    datadict_var_20c = cd.loadfiles(ens_20c,'tas')

    var_15c=datadict_var_15c['data']
    var_20c=datadict_var_20c['data']
    
    os.chdir('../')
   
    return var_15c,var_20c 
##tas
tas_15c,tas_20c=calc_read_ts('tas',do_calc=False)


##lonlat
lon=nc.getvar('DATA_tas/ts_tas_Amon_CanESM2_lowwarming15_r1i1p1_202101-210012.nc','lon');nlon=np.shape(lon)[0]
lat=nc.getvar('DATA_tas/ts_tas_Amon_CanESM2_lowwarming15_r1i1p1_202101-210012.nc','lat');nlat=np.shape(lat)[0]


##dims
nens=np.shape(tas_15c)[0];
nt=np.shape(tas_15c)[1];
#first convert tas to C, and set all positive values to zero

tas_15c=tas_15c+k_to_c 
tas_15c[tas_15c>0.]=0.
tas_20c=tas_20c+k_to_c
tas_20c[tas_20c>0.]=0.

################################################################
# 1b. Calculate tas_pi#
################################################################
calc_tas_pi=False

if calc_tas_pi:
    os.system('mkdir -p DATA_TAS_PI')
    os.chdir('DATA_TAS_PI')    
    os.system('ln -s /HOME/rms/DATA/CCC/CMIP5/piControl/mon/tas/tas_Amon_CanESM2_piControl_r1i1p1_*.nc .')
    # concat
    ens_tas_raw = cd.mkensemble('tas_Amon_CanESM2_piControl*')
    ens_tas_pi = cd.cat_exp_slices(ens_tas_raw)
else:
    os.chdir('DATA_TAS_PI')
    ens_tas_pi=cd.mkensemble('tas_Amon*')

datadict_tas_pi = cd.loadfiles(ens_tas_pi,'tas')
tas_pi=np.squeeze(datadict_tas_pi['data'])
os.chdir('../')

#first convert tas to C, and set all positive values to zero
tas_pi=tas_pi+k_to_c 
tas_pi[tas_pi>0.]=0.




################################################################
# 2. Calculate GM annual mean TAS timeseries Stabilization LEs#
################################################################
def calc_fdd(tas):
    ### in: tas(nt,nlat,nlon)
    ### out: fdd_clim(nlat,nlon)

    ### dims
    nt=np.shape(tas)[0];nlat=np.shape(tas)[1];nlon=np.shape(tas)[2]
    fdd_clim=np.zeros((nlat,nlon))
    ### number of days per calendar month
    ndayspermon=np.array((31,28,31,30,31,30,31,31,30,31,30,31))
    nyear=(nt+1)/12
    ### loop over all lons and lats
    for ilon in range(nlon):
        for ilat in range(nlat):
            ### sum over all months, all years, then divide by number of years        
            for it in range(nt):
                iy=it/12
                imon=it-(iy*12)                    
                fdd_clim[ilat,ilon]=fdd_clim[ilat,ilon]-(tas[it,ilat,ilon]*ndayspermon[imon])
    fdd_clim=fdd_clim/nyear            
    return fdd_clim


fdd_clim_15c=np.zeros((nens,nlat,nlon))
fdd_clim_20c=np.zeros((nens,nlat,nlon))
for iens in range(nens):
    print iens
    fdd_clim_15c[iens,:,:]=calc_fdd(np.squeeze(tas_15c[iens,:,:,:]))
    fdd_clim_20c[iens,:,:]=calc_fdd(np.squeeze(tas_20c[iens,:,:,:]))

fdd_clim_15c_em=np.mean(fdd_clim_15c,axis=0)
fdd_clim_20c_em=np.mean(fdd_clim_20c,axis=0)

fdd_clim_pi=calc_fdd(tas_pi)

fdd_clim_15c_em=fdd_clim_15c_em-fdd_clim_pi
fdd_clim_20c_em=fdd_clim_20c_em-fdd_clim_pi

######################################################
# 3. Plot clim
######################################################

def plot_clim(ax,lon,lat,clim):

    #bm    
    bmparams=dict(region='nps')
    bm=rpl.make_bm(ax,**bmparams)
    #cf            
    cfparams=dict(cint0=500,plot_co=False)
    cf=rpl.add_cf(bm,lon,lat,clim,**cfparams)
    return cf

#fig   TAS
fig, axs = plt.subplots(1,2, figsize=(8,8))
plt.subplots_adjust(wspace=0.0,hspace=0.05,right=0.83)    
#plot
cf=plot_clim(axs[0],lon,lat,fdd_clim_15c_em)
rpl.add_title(axs[0],'1.5$^\circ$C','a')

cf=plot_clim(axs[1],lon,lat,fdd_clim_20c_em)
rpl.add_title(axs[1],'2.0$^\circ$C','b')

#cbar
rpl.add_cb(axs[1],cf,units='k*day ',y0scale=2.5,hscale=0.5)
#suptitle
#fig.suptitle(('Surf. Temp. trends ' + str(year1) + '-' + str(year2)),y=0.72,fontsize=16)
#save
#rpl.mysavefig(fig,'tas_clim_Cansise_le_stab_2071-2100.png')
fig.savefig('fdd_clim_Cansise_le_stab_2100.pdf',dpi=300)

