"""
==========================

Get CMIP5 tas data, join the historical and RCP85 experiments (and time-slices within),
select a period of interest, then remap all models to a common 1x1 degree grid in space.

1. Import the tas data for historical and rcp85 experiments
2. Join all time-slices (within and across experiments)
3. Select the period of interest (1980-01 to 2013-12)
4. remap
5. Load the data (loading_tools) and make plots (plotting_tools)


"""
import os
import cmipdata as cd
import numpy as np
import matplotlib.pyplot as plt # for basic plotting
import nc as nc
import rms_plots as rpl
import rms_utils as rut
from scipy import stats
################settings
workdir=os.getcwd()
################################################################
# 1a. Calculate GM annual mean TAS timeseries Stabilization LEs#
################################################################
def calc_read_em(varnm,do_calc):

    srcdir_base='/HOME/rms/DATA/CCC/'        
    #srcdir_base='/raid/ra40/data/rms/DATA/CCC/'        
    os.system('mkdir -p DATA_' + varnm)
    os.chdir('DATA_' + varnm)

    if do_calc:
        os.system('/bin/rm *.nc')

        # all historical 
        os.system('ln -s ' + srcdir_base +'/LOWWARMING/lowwarming15/mon/'+varnm + '/*_2[0]??01-2[0,1]??12.nc .')
        os.system('ln -s ' + srcdir_base +'/LOWWARMING/lowwarming20/mon/'+varnm + '/*_2[0]??01-2[0,1]??12.nc .')
        # concat
        ens_15c = cd.mkensemble('*lowwarming15*')
        ens_15c = cd.cat_exp_slices(ens_15c)
        ens_20c = cd.mkensemble('*lowwarming20*')
        ens_20c = cd.cat_exp_slices(ens_20c)
        
        #climatology
        my_cdo_str='cdo -ymonmean seldate,2071-01-01,2100-12-31 {infile} {outfile}'
        ens_15c=cd.my_operator(ens_15c, my_cdo_str, output_prefix='clim_',delete=True)
        ens_20c=cd.my_operator(ens_20c, my_cdo_str, output_prefix='clim_',delete=True)               
        # em
    else:    
        os.chdir('DATA_' + varnm)
        ens_15c=cd.mkensemble('clim_tas_Amon*lowwarming15*',prefix='clim_')
        ens_20c=cd.mkensemble('clim_tas_Amon*lowwarming20*',prefix='clim_')

    datadict_tas_15c = cd.loadfiles(ens_15c,'tas')
    datadict_tas_20c = cd.loadfiles(ens_20c,'tas')

    var_15c=np.squeeze(datadict_tas_15c['data'])
    var_20c=np.squeeze(datadict_tas_20c['data'])
 
    os.chdir('../')
   
    return var_15c,var_20c 

##lonlat
#lon=nc.getvar('DATA_tas/15c_em.nc','lon');nlon=np.shape(lon)[0]
#lat=nc.getvar('DATA_tas/15c_em.nc','lat');nlat=np.shape(lat)[0]

##tas
tas_15c,tas_20c=calc_read_em('tas',do_calc=True)

################################################################
# 1b. Calculate tas_pi#
################################################################
calc_tas_pi=False

if calc_tas_pi:
    os.system('mkdir -p DATA_TAS_PI')
    os.chdir('DATA_TAS_PI')    
    os.system('ln -s /HOME/rms/DATA/CCC/CMIP5/piControl/mon/tas/tas_Amon_CanESM2_piControl_r1i1p1_*.nc .')
    # concat
    ens_tas_raw = cd.mkensemble('tas_Amon_CanESM2_piControl*')
    ens_tas = cd.cat_exp_slices(ens_tas_raw)
    # global and annual means
    my_cdo_str='cdo -yearmean {infile} {outfile}' 
    ens_tas_pi=cd.my_operator(ens_tas, my_cdo_str, output_prefix='am_',delete=True)
else:
    os.chdir('DATA_TAS_PI')
    ens_tas_pi=cd.mkensemble('am_tas_Amon*',prefix='am_')

datadict_tas_pi = cd.loadfiles(ens_tas_pi,'tas')
tas_pi=np.squeeze(datadict_tas_pi['data'])
os.chdir('../')


######################################################
# 2. Clim
######################################################
years_15c=np.arange(2021,2100+1)
years_20c=np.arange(2036,2100+1)

tas_pi_clim=np.mean(tas_pi,axis=0)
tas_15c_clim=np.mean(tas_15c[rut.find_nearest(years_15c,2071)::,:,:],axis=0)-tas_pi_clim
tas_20c_clim=np.mean(tas_20c[rut.find_nearest(years_20c,2071)::,:,:],axis=0)-tas_pi_clim



######################################################
# 3. Plot clim
######################################################

def plot_clim(ax,lon,lat,clim):

    #bm    
    bmparams=dict(region='nps')
    bm=rpl.make_bm(ax,**bmparams)
    #cf            
    cfparams=dict(cint0=2,plot_co=False)
    cf=rpl.add_cf(bm,lon,lat,clim,**cfparams)
    return cf

#fig   TAS
fig, axs = plt.subplots(1,2, figsize=(8,8))
plt.subplots_adjust(wspace=0.0,hspace=0.05,right=0.83)    
#plot
cf=plot_clim(axs[0],lon,lat,tas_15c_clim)
rpl.add_title(axs[0],'1.5$^\circ$C','a')

cf=plot_clim(axs[1],lon,lat,tas_20c_clim)
rpl.add_title(axs[1],'2.0$^\circ$C','b')

#cbar
rpl.add_cb(axs[1],cf,units='$^\circ$C ',y0scale=2.5,hscale=0.5)
#suptitle
#fig.suptitle(('Surf. Temp. trends ' + str(year1) + '-' + str(year2)),y=0.72,fontsize=16)
#save
#rpl.mysavefig(fig,'tas_clim_Cansise_le_stab_2071-2100.png')
fig.savefig('1_tas_clim_Cansise_le_stab_2071-2100.pdf',dpi=300)


        