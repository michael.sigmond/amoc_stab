"""
==========================

Get CMIP5 tas data, join the historical and RCP85 experiments (and time-slices within),
select a period of interest, then remap all models to a common 1x1 degree grid in space.

1. Import the tas data for historical and rcp85 experiments
2. Join all time-slices (within and across experiments)
3. Select the period of interest (1980-01 to 2013-12)
4. remap
5. Load the data (loading_tools) and make plots (plotting_tools)


"""
import os
import cmipdata as cd
import numpy as np
import matplotlib.pyplot as plt # for basic plotting
import nc as nc
import rms_plots as rpl
import rms_utils as rut
from scipy import stats
################settings
calc_tas_pi=False
workdir=os.getcwd()
######################################################
# 1a. Calculate GM annual mean TAS timeseries LE
######################################################
def calc_read_em(varnm,do_calc):

    srcdir_base='/HOME/rms/DATA/CCC/'        
    #srcdir_base='/raid/ra40/data/rms/DATA/CCC/'        
    os.system('mkdir -p DATA_' + varnm)
    os.chdir('DATA_' + varnm)

    if do_calc:
        os.system('/bin/rm *.nc')

        # all historical 
        os.system('ln -s ' + srcdir_base +'/CanSISE/historical-r*/mon/'+varnm + '/*r*i1*.nc .')
        os.system('ln -s ' + srcdir_base +'/LOWWARMING/lowwarming15/mon/'+varnm + '/*_2[0]??01-2[0,1]??12.nc .')
        # concat
        ens_his = cd.mkensemble('*historical-r*')
        ens_his = cd.cat_exp_slices(ens_his)
        ens_15c = cd.mkensemble('*lowwarming15*')
        ens_15c = cd.cat_exp_slices(ens_15c)
        
        #annual mean
        my_cdo_str='cdo -yearmean {infile} {outfile}'
        ens_his=cd.my_operator(ens_his, my_cdo_str, output_prefix='am_',delete=True)
        ens_15c=cd.my_operator(ens_15c, my_cdo_str, output_prefix='am_',delete=True)
               
        # em
        os.system('cdo -ensmean am*historical-r*.nc his_em.nc')
        os.system('cdo -ensmean am*lowwarming15*.nc 15c_em.nc')
        os.system('/bin/rm am_*historical-r*.nc am_*lowwarming15*.nc')        

    var_his=nc.getvar('his_em.nc',varnm) 
    var_15c=nc.getvar('15c_em.nc',varnm) 
 
    os.chdir('../')
   
    return var_his,var_15c 

##lonlat
lon=nc.getvar('DATA_tas/his_em.nc','lon');nlon=np.shape(lon)[0]
lat=nc.getvar('DATA_tas/his_em.nc','lat');nlat=np.shape(lat)[0]

##vars
tas_his,tas_15c=calc_read_em('tas',do_calc=False)
pr_his,pr_15c=calc_read_em('pr',do_calc=False)
sic_his,sic_15c=calc_read_em('sic',do_calc=False)




######################################################
# 1c. Post
######################################################

pr_his=pr_his*86400
pr_15c=pr_15c*86400

######################################################
# 2. Trend
######################################################
#years
years_15c=np.arange(2021,2100+1)
year1=2046
year2=2100
nyear=year2-year1
iyear1=rut.find_nearest(years_15c,year1)
iyear2=rut.find_nearest(years_15c,year2)

#trends
tas_15c_trend=np.zeros((nlat,nlon))
tas_15c_trend_pval=np.zeros((nlat,nlon))

pr_15c_trend=np.zeros((nlat,nlon))
pr_15c_trend_pval=np.zeros((nlat,nlon))

sic_15c_trend=np.zeros((nlat,nlon))
sic_15c_trend_pval=np.zeros((nlat,nlon))



for ilat in range(nlat):
    for ilon in range(nlon):
        tas_15c_trend[ilat,ilon]     =stats.linregress(years_15c[iyear1:iyear2+1],tas_15c[iyear1:iyear2+1,ilat,ilon].squeeze())[0]*nyear
        tas_15c_trend_pval[ilat,ilon]=stats.linregress(years_15c[iyear1:iyear2+1],tas_15c[iyear1:iyear2+1,ilat,ilon].squeeze())[3]

        pr_15c_trend[ilat,ilon]     =stats.linregress(years_15c[iyear1:iyear2+1],pr_15c[iyear1:iyear2+1,ilat,ilon].squeeze())[0]*nyear
        pr_15c_trend_pval[ilat,ilon]=stats.linregress(years_15c[iyear1:iyear2+1],pr_15c[iyear1:iyear2+1,ilat,ilon].squeeze())[3]

        sic_15c_trend[ilat,ilon]     =stats.linregress(years_15c[iyear1:iyear2+1],sic_15c[iyear1:iyear2+1,ilat,ilon].squeeze())[0]*nyear
        sic_15c_trend_pval[ilat,ilon]=stats.linregress(years_15c[iyear1:iyear2+1],sic_15c[iyear1:iyear2+1,ilat,ilon].squeeze())[3]


######################################################
# 3. Plot trend
######################################################

def plot_trend(ax,lon,lat,trend,trend_pval,varname):

    #bm    
    if varname=='sic':bmparams=dict(landmask=True,region='nps')
    else: bmparams=dict(landmask=False,region='glob_rob')
    bm=rpl.make_bm(ax,**bmparams)

    #cf    
    if varname=='tas': cint=0.2
    if varname=='pr': cint=0.1
    if varname=='sic': cint=1
        
    cfparams=dict(cint0=cint,plot_co=False)
    cf=rpl.add_cf(bm,lon,lat,trend,**cfparams)

    #significance         
    sig=np.zeros((nlat,nlon))
    sig[trend_pval<0.05]=0.2
    rpl.add_sc(bm,lon,lat,sig)

    return cf
    

#fig   TAS
fig, axs = plt.subplots(1,1, figsize=(8,8))
plt.subplots_adjust(wspace=0.0,hspace=0.05,right=0.83)    
#plot
cf=plot_trend(axs,lon,lat,tas_15c_trend,tas_15c_trend_pval,'tas')
#cbar
rpl.add_cb(axs,cf,units='$^\circ$C per ' + str(nyear) + ' years',y0scale=2.5,hscale=0.5)
#suptitle
fig.suptitle(('Surf. Temp. trends ' + str(year1) + '-' + str(year2)),y=0.72,fontsize=16)
#save
rpl.mysavefig(fig,('tas_trend_' + str(year1) + '-' + str(year2) + '.png'))


#fig   PR
fig, axs = plt.subplots(1,1, figsize=(8,8))
plt.subplots_adjust(wspace=0.0,hspace=0.05,right=0.83)    
#plot
cf=plot_trend(axs,lon,lat,pr_15c_trend,pr_15c_trend_pval,'pr')
#cbar
rpl.add_cb(axs,cf,units='mm/day per ' + str(nyear) + ' years',y0scale=2.5,hscale=0.5)
#suptitle
fig.suptitle(('Precipitation trends ' + str(year1) + '-' + str(year2)),y=0.72,fontsize=16)
#save
rpl.mysavefig(fig,('pr_trend_' + str(year1) + '-' + str(year2) + '.png'))

#fig   SIC
fig, axs = plt.subplots(1,1, figsize=(8,8))
plt.subplots_adjust(wspace=0.0,hspace=0.05,right=0.83)    
#plot
cf=plot_trend(axs,lon,lat,sic_15c_trend,sic_15c_trend_pval,'sic')
#cbar
rpl.add_cb(axs,cf,units='% per ' + str(nyear) + ' years',y0scale=2.5,hscale=0.5)
#suptitle
fig.suptitle(('SIC trends ' + str(year1) + '-' + str(year2)),y=0.72,fontsize=16)
#save
rpl.mysavefig(fig,('sic_trend_' + str(year1) + '-' + str(year2) + '.png'))


