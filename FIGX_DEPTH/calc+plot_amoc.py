"""
==========================
MS, 20190404

- Get 2D msftmz (AMOC) data, Calculate maximum of annual mean stream function with CDO

- Result: while numerical values are slightly different than in RTD, year-to-year variations are indistinguishable
- Result: reproduced plot that was made with 2_calc+plot_amoc_old.py (which calculated AMOC strength not with CDO but in python)
- 2_calc+plot_amoc_old2.py: changed order of calculation (maximum of monthly fields, then annual mean); this made a difference
                              and brought the results out of line of that in the RTD


"""
import os
import cmipdata as cd
import numpy as np
import matplotlib.pyplot as plt # for basic plotting
import nc as nc
import rms_plots as rpl
import rms_utils as rut
from scipy import stats
import matplotlib as mpl
import scipy.interpolate as intplt
from matplotlib.ticker import MultipleLocator
################settings
mpl.style.reload_library()
plt.style.use('rms_paper')

rundir='/HOME/rms/SCRIPTS/6_OTHER/6PARIS-STAB/7DEPTH/';os.chdir(rundir)
######################################################
# 1a. Calculate AMOC timeseries LE
######################################################
srcdir_base='/HOME/rms/DATA/CCC/'        
#srcdir_base='/raid/ra40/data/rms/DATA/CCC/'        
do_calc=True
varnm='msftmyz'
if do_calc:
    os.system('mkdir -p DATA_{}'.format(varnm))
    os.chdir('DATA_{}'.format(varnm))
    os.system('/bin/rm *.nc')

    # all historical 
    os.system('ln -s ' + srcdir_base +'/CanSISE/historical-r*/mon/'+varnm+'/*.nc .')
    os.system('ln -s ' + srcdir_base +'/LOWWARMING/lowwarming15/mon/'+varnm+'/*.nc .')
    os.system('ln -s ' + srcdir_base +'/LOWWARMING/lowwarming20/mon/'+varnm+'/*.nc .')
    os.system('ln -s ' + srcdir_base +'/LOWWARMING/lowwarming30/mon/'+varnm+'a/*.nc .')

    # concat
    ens_his = cd.mkensemble('*historical-r*')
    ens_his = cd.cat_exp_slices(ens_his)
    #ens_15c1 = cd.mkensemble('*lowwarming15*_r*i1p1_20??01-2???12.nc') #all up to 2100
    #ens_15c1 = cd.cat_exp_slices(ens_15c1)
    ens_15c2 = cd.mkensemble('*lowwarming15*_r[1-5]i1p1_2[1-6]??01-2???12.nc') #1-5 past 2100
    ens_15c2 = cd.cat_exp_slices(ens_15c2)
    #ens_20c1 = cd.mkensemble('*lowwarming20*_r*i1p1_20??01-2???12.nc') #all up to 2100
    #ens_20c1 = cd.cat_exp_slices(ens_20c1)
    ens_20c2 = cd.mkensemble('*lowwarming20*_r[1-5]i1p1_2[1-6]??01-2???12.nc') #1-5 past 2100
    ens_20c2 = cd.cat_exp_slices(ens_20c2)
    #ens_30c1 = cd.mkensemble('*lowwarming30*_r*i1p1_20??01-2???12.nc') #all up to 2100
    #ens_30c1 = cd.cat_exp_slices(ens_30c1)
    ens_30c2 = cd.mkensemble('*lowwarming30*_r[1-5]i1p1_2[1-6]??01-2???12.nc') #1-5 past 2100
    ens_30c2 = cd.cat_exp_slices(ens_30c2)


    # annual global means            
    print ('Calculating annual mean psi')
    my_cdo_str='cdo -yearmean {infile} {outfile}' 
    ens_his_am=cd.my_operator(ens_his, my_cdo_str, output_prefix='am_',delete=True)
    ens_15c2_am=cd.my_operator(ens_15c2, my_cdo_str, output_prefix='am_',delete=True)
    ens_20c2_am=cd.my_operator(ens_20c2, my_cdo_str, output_prefix='am_',delete=True)
    ens_30c2_am=cd.my_operator(ens_30c2, my_cdo_str, output_prefix='am_',delete=True)
    # Select NH, Atlantic basin            
    print ('Select Atlantic basin')
    my_ncks_str='ncks -d basin,0 -d lat,30,191 {infile} {outfile}' ; prefix='nh_'
    ens_his=cd.my_operator(ens_his_am, my_ncks_str, output_prefix='h_',delete=True)
    ens_15c2=cd.my_operator(ens_15c2_am, my_ncks_str, output_prefix='h_',delete=True)
    ens_20c2=cd.my_operator(ens_20c2_am, my_ncks_str, output_prefix='h_',delete=True)
    my_ncks_str='ncks -d lat,30,191 {infile} {outfile}' ; 
    ens_30c2=cd.my_operator(ens_30c2_am, my_ncks_str, output_prefix='nh_',delete=True)
    # Select NH, Atlantic basin            
    print ('getting rid of the basin dimension')
    my_ncks_str='ncwa -a basin {infile} {outfile}' ;
    ens_his=cd.my_operator(ens_his, my_ncks_str, output_prefix='n',delete=True)
    ens_15c2=cd.my_operator(ens_15c2, my_ncks_str, output_prefix='n',delete=True)
    ens_20c2=cd.my_operator(ens_20c2, my_ncks_str, output_prefix='n',delete=True)

else:
    os.chdir('DATA_{}'.format(varnm))
    ens_his = cd.mkensemble('nh_am_*historical-r*',prefix='nh_am_')
    ens_15c2 = cd.mkensemble('nh_am_*lowwarming15*_r[1-5]i1p1_2[1-6]??01-2???12.nc',prefix='nh_am_') #1-5 past 2100
    ens_20c2 = cd.mkensemble('nh_am_*lowwarming20*_r[1-5]i1p1_2[1-6]??01-2???12.nc',prefix='nh_am_') #1-5 past 2100
    ens_30c2 = cd.mkensemble('nh_am_*lowwarming30*_r[1-5]i1p1_2[1-6]??01-2???12.nc',prefix='nh_am_') #1-5 past 2100

msft_his=cd.loadfiles(ens_his,varnm)['data']/1.025e9
#msft_15c1=cd.loadfiles(ens_15c1,varnm)['data']/1.025e9
#msft_20c1=cd.loadfiles(ens_20c1,varnm)['data']/1.025e9
#msft_30c1=cd.loadfiles(ens_30c1,'msftmyza')['data']/1.025e9

msft_15c2=cd.loadfiles(ens_15c2,varnm)['data']/1.025e9
msft_20c2=cd.loadfiles(ens_20c2,varnm)['data']/1.025e9
msft_30c2=cd.loadfiles(ens_30c2,'msftmyza')['data']/1.025e9

lat=cd.loadfiles(ens_his,varnm)['dimensions']['lat'];
levs=cd.loadfiles(ens_his,varnm)['dimensions']['lev'];

os.chdir('../')
#Years#############################################
years_his=np.arange(1950,2100+1)
years_15c1=np.arange(2021,2100+1)
years_20c1=np.arange(2036,2100+1)
years_30c1=np.arange(2061,2100+1)
years2=np.arange(2101,2600+1)


######################################################
# 2. climatological mean at 26N 
######################################################
ilat=rut.find_nearest(lat,26.5)
msft_his_clim=np.mean(msft_his[:,0:50,:,ilat],axis=1).squeeze() # ens,lev
msft_15c_clim=np.mean(msft_15c2[:,-50::,:,ilat],axis=1).squeeze() # ens,lev
msft_20c_clim=np.mean(msft_20c2[:,-50::,:,ilat],axis=1).squeeze() # ens,lev
msft_30c_clim=np.mean(msft_30c2[:,-50::,:,ilat],axis=1).squeeze() # ens,lev

msft_his_clim_em=np.mean(msft_his_clim,axis=0) # lev
msft_15c_clim_em=np.mean(msft_15c_clim,axis=0) # lev
msft_20c_clim_em=np.mean(msft_20c_clim,axis=0) # lev
msft_30c_clim_em=np.mean(msft_30c_clim,axis=0) # lev








######################################################
# 3. Plot 
######################################################

fig1, axs = plt.subplots(1,1, figsize=(8,8)); 
fig1.subplots_adjust(bottom=0.5,right=0.5,wspace=0)

#1950-2100############################################
ax=axs

ax.set_xlim([-3.2, 16]) 
#ax.set_xticks(np.arange(1950,2125,25))
ax.xaxis.set_major_locator(MultipleLocator(5))
ax.xaxis.set_minor_locator(MultipleLocator(1)) 

ax.set_ylim(10,3000) 
#ax.set_yticks((3000,1000,300, 100, 30,10))  
#ax.set_yticklabels((3000,1000, 300, 100, 30,10))  
#ax.set_yscale('log')
ax.invert_yaxis()
ax.yaxis.set_major_locator(MultipleLocator(500))
ax.yaxis.set_minor_locator(MultipleLocator(100))
ax.set_ylabel('Depth (m)')
ax.set_xlabel('Atlantic streamfunction at 26.6$^\circ$N (Sv)')
####### plot
##his##
kwargs={'linewidth': 0.5, 'color': 'gray'}
ax.plot(np.mean(msft_his_clim,axis=0),levs,**kwargs)

    
##15c##
kwargs={'linewidth': 0.5, 'color': 'deepskyblue'}
ax.plot(np.mean(msft_15c_clim,axis=0),levs,**kwargs)
#kwargs={'color':'deepskyblue', 'alpha': 0.25, 'edgecolor':'none'}
#ax.fill_between(years_15c1,np.min(amoc_15c1,axis=0), np.max(amoc_15c1,axis=0),**kwargs)

##20c##
kwargs={'linewidth': 0.5, 'color': 'orange'}
ax.plot(np.mean(msft_20c_clim,axis=0),levs,**kwargs)

#ax.plot(years_20c1,np.mean(msft_20c_clim,axis=0),**kwargs)
#kwargs={'color':'orange', 'alpha': 0.25, 'edgecolor':'none'}
#ax.fill_between(years_20c1,np.min(amoc_20c1,axis=0), np.max(amoc_20c1,axis=0),**kwargs)

##30c##
kwargs={'linewidth': 0.5, 'color': 'red'}
ax.plot(np.mean(msft_30c_clim,axis=0),levs,**kwargs)
#kwargs={'color':'red', 'alpha': 0.25, 'edgecolor':'none'}
#ax.fill_between(years_30c1,np.min(amoc_30c1,axis=0), np.max(amoc_30c1,axis=0),**kwargs)

##linesc##
#ax.axhline(y=0,color="black",linewidth=0.5)


##legend##

ax.text(-3,500,'HIST (1950-1999)',color= 'gray')
ax.text(-3,650,'1.5$^\circ$C (2551-2600)',color= 'deepskyblue')
ax.text(-3,800,'2.0$^\circ$C (2551-2600)',color= 'orange')
ax.text(-3,950,'3.0$^\circ$C (2551-2600)',color= 'red')

#ax.text(1955,12.5,'3.0$^\circ$C',color= 'red')
rpl.mysavefig(fig1,'msft.png')


######################################################
# 4. Depth of AMOC 
######################################################
def calc_depth5(var,levs):
  levs_indices = np.where(np.logical_and(levs >= 1000, levs <= 3000))[0]
  levs_fine = 1000. + np.array(range(1500)) 

  cub_spline = intplt.CubicSpline(levs[levs_indices], var[levs_indices])
  curr_slicef = cub_spline(levs_fine)
  depth = np.float(levs_fine[rut.find_nearest(curr_slicef, 0)])
  return depth

def calc_depthmax(var,levs):
  levs_indices = np.where(np.logical_and(levs >= 400, levs <= 1100))[0]
  levs_fine = 400. + np.array(range(700)) 

  cub_spline = intplt.CubicSpline(levs[levs_indices], var[levs_indices])
  curr_slicef = cub_spline(levs_fine)
  depth = np.float(levs_fine[rut.find_nearest(curr_slicef, np.max(curr_slicef))])
  return depth

#d_clim=calc_depth5(msft_his_clim_em,levs)
#d_15c=calc_depth5(msft_15c_clim_em,levs)
#d_20c=calc_depth5(msft_20c_clim_em,levs)
#d_30c=calc_depth5(msft_30c_clim_em,levs)

d_his=calc_depthmax(msft_his_clim_em,levs)
d_15c=calc_depthmax(msft_15c_clim_em,levs)
d_20c=calc_depthmax(msft_20c_clim_em,levs)
d_30c=calc_depthmax(msft_30c_clim_em,levs)


text_file = open("depth_stabilization2_equi.txt", "w")

text_file.write('Depth his:'+ "%5.3f"%(d_his) + '\n\n')
text_file.write('Depth 15c:'+ "%5.3f"%(d_15c) + '\n')
text_file.write('Depth 15c-his(*2):'+ "%5.3f"%((d_15c-d_his)*200/d_his) + '%\n\n')

text_file.write('Depth 20c:'+ "%5.3f"%(d_20c) + '\n')
text_file.write('Depth 20c-his(*2):'+ "%5.3f"%((d_20c-d_his)*200/d_his) + '%\n\n')

text_file.write('Depth 30c:'+ "%5.3f"%(d_30c) + '\n')
text_file.write('Depth 30c-his(*2):'+ "%5.3f"%((d_30c-d_his)*200/d_his) + '%\n\n')

text_file.write('Depth 20c-15c(*2):'+ "%5.3f"%((d_20c-d_15c)*200/d_15c) + '%\n')
text_file.write('Depth 30c-15c(*2):'+ "%5.3f"%((d_30c-d_15c)*200/d_15c) + '%\n')

text_file.close()
























