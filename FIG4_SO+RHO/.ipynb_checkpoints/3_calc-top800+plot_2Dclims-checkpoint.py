"""
==========================
MS, 20191003

Input: 
  - SO/RHO (lon,lat,lev,year) [experiments: PI, historical, lowwarmingXX]

Output: 
  -Data: SO/RHO_top800 (lon,lat,year) 
  -Data: SO/RHO_atlzm  (lat,lev,year) 
  -Plots: Decadal mean upper ocean mean (lon,lat, year) and atlantic ZM (lat, lev, year) Salinity or rho0 


"""
import os
import cmipdata as cd
import numpy as np
import matplotlib.pyplot as plt # for basic plotting
import rms_plots as rpl
import rms_utils as rut
################settings
rundir='/HOME/rms/SCRIPTS/6_OTHER/6PARIS-STAB/4SO/';os.chdir(rundir)

######################################################
# 1a. Calculate GM annual mean TAS timeseries LE
######################################################
srcdir_base='/HOME/rms/DATA/CCC/'        
#srcdir_base='/raid/ra40/data/rms/DATA/CCC/'        
do_calc=False
var='rho0'
#var='so'


if do_calc:
    os.system('mkdir -p DATA_{}'.format(var))
    os.chdir('DATA_{}'.format(var))
    os.system('/bin/rm *.nc')

    # link files 
    os.system('ln -s ' + srcdir_base +'/CMIP5/piControl/mon/'+var+'/*r1i1*.nc .')
    os.system('ln -s ' + srcdir_base +'/CanSISE/historical-r1/mon/'+var+'/*r[1-5]i1*.nc .')
    os.system('ln -s ' + srcdir_base +'/LOWWARMING/lowwarming15/mon/'+var+'/*r[1-5]i1*.nc .')
    os.system('ln -s ' + srcdir_base +'/LOWWARMING/lowwarming20/mon/'+var+'/*r[1-5]i1*.nc .')
    os.system('ln -s ' + srcdir_base +'/LOWWARMING/lowwarming30/mon/'+var+'/*r[1-5]i1*.nc .')
      
    # make ensemble
    ens_pic  = cd.mkensemble('*piControl*',prefix='am_') #lon,lat,z,year    
    ens_his = cd.mkensemble('*historical-r*',prefix='am_') #lon,lat,z,year
    ens_15c = cd.mkensemble('*lowwarming15*',prefix='am_') #lon,lat,z,year
    ens_20c = cd.mkensemble('*lowwarming20*',prefix='am_') #lon,lat,z,year
    ens_30c = cd.mkensemble('*lowwarming30*',prefix='am_') #lon,lat,z,year

    # vertical mean top800
    my_cdo_str='cdo  -sellevel,5,15,25.1,35.3,45.6,56.1,66.8,77.7,88.9,100.5,112.6,125.5,139.5,154.9,172.2,192.1,215.4,243.1,276.6,317.7,368.6,431.9,510.5,607.7,726.8 {infile} {outfile}'      
    #my_cdo_str='cdo  -sellevel,5,15,25.1,35.3,45.6,56.1,66.8,77.7,88.9,100.5,112.6,125.5,139.5,154.9,172.2,192.1,215.4,243.1,276.6 {infile} {outfile}'      
 
    ens_pic_8=cd.my_operator(ens_pic, my_cdo_str, output_prefix='8_',delete=False) #lon,lat,year
    ens_his_8=cd.my_operator(ens_his, my_cdo_str, output_prefix='8_',delete=False) #lon,lat,year
    ens_15c_8=cd.my_operator(ens_15c, my_cdo_str, output_prefix='8_',delete=False) #lon,lat,year
    ens_20c_8=cd.my_operator(ens_20c, my_cdo_str, output_prefix='8_',delete=False) #lon,lat,year
    ens_30c_8=cd.my_operator(ens_30c, my_cdo_str, output_prefix='8_',delete=False) #lon,lat,year

    my_cdo_str='cdo -vertmean {infile} {outfile}'
    ens_pic_top8=cd.my_operator(ens_pic_8, my_cdo_str, output_prefix='top',delete=True) #lon,lat,year
    ens_his_top8=cd.my_operator(ens_his_8, my_cdo_str, output_prefix='top',delete=True) #lon,lat,year
    ens_15c_top8=cd.my_operator(ens_15c_8, my_cdo_str, output_prefix='top',delete=True) #lon,lat,year
    ens_20c_top8=cd.my_operator(ens_20c_8, my_cdo_str, output_prefix='top',delete=True) #lon,lat,year
    ens_30c_top8=cd.my_operator(ens_30c_8, my_cdo_str, output_prefix='top',delete=True) #lon,lat,year

    # top level only
    my_cdo_str='cdo  -sellevel,5 {infile} {outfile}'      
    ens_pic_top=cd.my_operator(ens_pic, my_cdo_str, output_prefix='top_',delete=False) #lon,lat,year
    ens_his_top=cd.my_operator(ens_his, my_cdo_str, output_prefix='top_',delete=False) #lon,lat,year
    ens_15c_top=cd.my_operator(ens_15c, my_cdo_str, output_prefix='top_',delete=False) #lon,lat,year
    ens_20c_top=cd.my_operator(ens_20c, my_cdo_str, output_prefix='top_',delete=False) #lon,lat,year
    ens_30c_top=cd.my_operator(ens_30c, my_cdo_str, output_prefix='top_',delete=False) #lon,lat,year

    # Atlantic zonal mean
    my_cdo_str='cdo -zonmean -ifthen /HOME/rms/DATA/MASKS/OCEAN/atlantic_mask.nc {infile} {outfile}'
    ens_pic_atlzm=cd.my_operator(ens_pic, my_cdo_str, output_prefix='atlzm_',delete=True) #lon,lat,year
    ens_his_atlzm=cd.my_operator(ens_his, my_cdo_str, output_prefix='atlzm_',delete=True) #lat,z,year
    ens_15c_atlzm=cd.my_operator(ens_15c, my_cdo_str, output_prefix='atlzm_',delete=True) #lat,z,year
    ens_20c_atlzm=cd.my_operator(ens_20c, my_cdo_str, output_prefix='atlzm_',delete=True) #lat,z,year
    ens_30c_atlzm=cd.my_operator(ens_30c, my_cdo_str, output_prefix='atlzm_',delete=True) #lat,z,year

    # Atlantic zonal mean and vertical mean
    #my_cdo_str='cdo -vertmean -sellevel,5,15,25.1,35.3,45.6,56.1,66.8,77.7,88.9,100.5,112.6,125.5,139.5,154.9,172.2,192.1,215.4,243.1,276.6,317.7,368.6,431.9,510.5,607.7,726.8 {infile} {outfile}'
    #ens_pic_atlzm_top=cd.my_operator(ens_pic_atlzm, my_cdo_str, output_prefix='top8_',delete=False) #lon,lat,year
    #ens_his_atlzm_top=cd.my_operator(ens_his_atlzm, my_cdo_str, output_prefix='top8_',delete=False) #lat,z,year
    #ens_15c_atlzm_top=cd.my_operator(ens_15c_atlzm, my_cdo_str, output_prefix='top8_',delete=False) #lat,z,year
    #ens_20c_atlzm_top=cd.my_operator(ens_20c_atlzm, my_cdo_str, output_prefix='top8_',delete=False) #lat,z,year

else:
    os.chdir('DATA_{}'.format(var))
    ens_pic_top8=cd.mkensemble('top8_am_*_piControl*',prefix='top8_am_')    
    ens_his_top8=cd.mkensemble('top8_am_*_historical-r*',prefix='top8_am_')
    ens_15c_top8=cd.mkensemble('top8_am_*_lowwarming15*',prefix='top8_am_')
    ens_20c_top8=cd.mkensemble('top8_am_*_lowwarming20*',prefix='top8_am_')
    ens_30c_top8=cd.mkensemble('top8_am_*_lowwarming30*',prefix='top8_am_')

    ens_pic_top=cd.mkensemble('top_am_*_piControl*',prefix='top_am_')    
    ens_his_top=cd.mkensemble('top_am_*_historical-r*',prefix='top_am_')
    ens_15c_top=cd.mkensemble('top_am_*_lowwarming15*',prefix='top_am_')
    ens_20c_top=cd.mkensemble('top_am_*_lowwarming20*',prefix='top_am_')
    ens_30c_top=cd.mkensemble('top_am_*_lowwarming30*',prefix='top_am_')

    ens_pic_atlzm=cd.mkensemble('atlzm_am_*_piControl*',prefix='atlzm_am_')
    ens_his_atlzm=cd.mkensemble('atlzm_am_*_historical-r*',prefix='atlzm_am_')
    ens_15c_atlzm=cd.mkensemble('atlzm_am_*_lowwarming15*',prefix='atlzm_am_')
    ens_20c_atlzm=cd.mkensemble('atlzm_am_*_lowwarming20*',prefix='atlzm_am_')
    ens_30c_atlzm=cd.mkensemble('atlzm_am_*_lowwarming30*',prefix='atlzm_am_')    

datadict_pic_top8 = cd.loadfiles(ens_pic_top8,var)
datadict_his_top8 = cd.loadfiles(ens_his_top8,var)
datadict_15c_top8 = cd.loadfiles(ens_15c_top8,var)
datadict_20c_top8 = cd.loadfiles(ens_20c_top8,var)
datadict_30c_top8 = cd.loadfiles(ens_30c_top8,var)

datadict_pic_top = cd.loadfiles(ens_pic_top,var)
datadict_his_top = cd.loadfiles(ens_his_top,var)
datadict_15c_top = cd.loadfiles(ens_15c_top,var)
datadict_20c_top = cd.loadfiles(ens_20c_top,var)
datadict_30c_top = cd.loadfiles(ens_30c_top,var)

datadict_pic_atlzm = cd.loadfiles(ens_pic_atlzm,var)
datadict_his_atlzm = cd.loadfiles(ens_his_atlzm,var)
datadict_15c_atlzm = cd.loadfiles(ens_15c_atlzm,var)
datadict_20c_atlzm = cd.loadfiles(ens_20c_atlzm,var)
datadict_30c_atlzm = cd.loadfiles(ens_30c_atlzm,var)

x_pic_top8=datadict_pic_top8['data'].squeeze()
x_his_top8=datadict_his_top8['data'].squeeze()
x_15c_top8=datadict_15c_top8['data'].squeeze()
x_20c_top8=datadict_20c_top8['data'].squeeze()
x_30c_top8=datadict_30c_top8['data'].squeeze()

x_pic_top=datadict_pic_top['data'].squeeze()
x_his_top=datadict_his_top['data'].squeeze()
x_15c_top=datadict_15c_top['data'].squeeze()
x_20c_top=datadict_20c_top['data'].squeeze()
x_30c_top=datadict_30c_top['data'].squeeze()


x_pic_atlzm=datadict_pic_atlzm['data'].squeeze(); #x_pic_atlzm[x_pic_atlzm>1e20]=np.nan
x_his_atlzm=datadict_his_atlzm['data'].squeeze()
x_15c_atlzm=datadict_15c_atlzm['data'].squeeze()
x_20c_atlzm=datadict_20c_atlzm['data'].squeeze()
x_30c_atlzm=datadict_30c_atlzm['data'].squeeze()


#dims
lon=datadict_his_top['dimensions']['lon'];  nlon=lon.size
lat=datadict_his_top['dimensions']['lat'];  nlat=lat.size
lev=datadict_his_atlzm['dimensions']['lev']; nlev=lev.size

os.chdir('../')

######################################################
# 2. years, picontrol climatologies
######################################################
years_his=np.arange(1950,2100+1)
years_15c=np.arange(2021,2600+1)
years_20c=np.arange(2036,2600+1)
years_30c=np.arange(2061,2600+1)

x_piclim_top8=np.mean(x_pic_top8,axis=0) #lon,lat,piclim
x_piclim_top=np.mean(x_pic_top,axis=0) #lon,lat,piclim
x_piclim_atlzm=np.mean(x_pic_atlzm,axis=0) #lon,lat,piclim

######################################################
# 3. Plot 
######################################################

##########FIG

for year in np.arange(1960,2600,10):

  ncol=4; nrow=3
  fig, axs = plt.subplots(nrow,ncol, figsize=(ncol*3,nrow*2.5)); 
  plt.subplots_adjust(wspace=0.2)

  ##########ROW1+2 (lat-lon)
  bmparams=dict(region='atl')
  cfparams=dict(cint0=0.2,plot_co=False)


  ####ROW1 (lat-lon, top)
  #####hist
  ax=axs[0,0]   
  if year<2100:
    #var
    i=rut.find_nearest(years_his,year-5)
    x=np.mean(np.mean(x_his_top[:,i:(i+11),:,:],axis=0),axis=0)-x_piclim_top
    #plot
    bm=rpl.make_bm(ax,**bmparams)
    cf=rpl.add_cf(bm,lon,lat,x,**cfparams)
    #title
    rpl.add_title(ax,'hist-rcp, {} top 10m'.format(year))

  #####15c
  ax=axs[0,1]   
  if year>2020:
    #var
    i=rut.find_nearest(years_15c,year-5)
    x=np.mean(np.mean(x_15c_top[:,i:(i+11),:,:],axis=0),axis=0)-x_piclim_top
    #plot
    bm=rpl.make_bm(ax,**bmparams)
    cf=rpl.add_cf(bm,lon,lat,x,**cfparams)
    #title
    rpl.add_title(ax,'1.5C, {} top 10m'.format(year))

  #####20c
  ax=axs[0,2]   
  if year>2040:
    #var
    i=rut.find_nearest(years_20c,year-5)
    x=np.mean(np.mean(x_20c_top[:,i:(i+11),:,:],axis=0),axis=0)-x_piclim_top
    #plot
    bm=rpl.make_bm(ax,**bmparams)
    cf=rpl.add_cf(bm,lon,lat,x,**cfparams)
    #title
    rpl.add_title(ax,'2.0C, {} 10m'.format(year))

  #####30c
  ax=axs[0,3]   
  if year>2060:
    #var
    i=rut.find_nearest(years_30c,year-5)
    x=np.mean(np.mean(x_30c_top[:,i:(i+11),:,:],axis=0),axis=0)-x_piclim_top
    #plot
    bm=rpl.make_bm(ax,**bmparams)
    cf=rpl.add_cf(bm,lon,lat,x,**cfparams)
    #title
    rpl.add_title(ax,'3.0C, {} top 10m'.format(year))
    #cbar
    rpl.add_cb(ax,cf)


  ####ROW2 (lat-lon, top 300)
  #####hist
  ax=axs[1,0]   
  if year<2100:
    #var
    i=rut.find_nearest(years_his,year-5)
    x=np.mean(np.mean(x_his_top8[:,i:(i+11),:,:],axis=0),axis=0)-x_piclim_top8
    #plot
    bm=rpl.make_bm(ax,**bmparams)
    cf=rpl.add_cf(bm,lon,lat,x,**cfparams)
    #title
    rpl.add_title(ax,'hist-rcp, {} top 300m'.format(year))

  #####15c
  ax=axs[1,1]   
  if year>2020:
    #var
    i=rut.find_nearest(years_15c,year-5)
    x=np.mean(np.mean(x_15c_top8[:,i:(i+11),:,:],axis=0),axis=0)-x_piclim_top8
    #plot
    bm=rpl.make_bm(ax,**bmparams)
    cf=rpl.add_cf(bm,lon,lat,x,**cfparams)
    #title
    rpl.add_title(ax,'1.5C, {} top 300m'.format(year))

  #####20c
  ax=axs[1,2]   
  if year>2040:
    #var
    i=rut.find_nearest(years_20c,year-5)
    x=np.mean(np.mean(x_20c_top8[:,i:(i+11),:,:],axis=0),axis=0)-x_piclim_top8
    #plot
    bm=rpl.make_bm(ax,**bmparams)
    cf=rpl.add_cf(bm,lon,lat,x,**cfparams)
    #title
    rpl.add_title(ax,'2.0C, {} 300m'.format(year))

  #####30c
  ax=axs[1,3]   
  if year>2060:
    #var
    i=rut.find_nearest(years_30c,year-5)
    x=np.mean(np.mean(x_30c_top8[:,i:(i+11),:,:],axis=0),axis=0)-x_piclim_top8
    #plot
    bm=rpl.make_bm(ax,**bmparams)
    cf=rpl.add_cf(bm,lon,lat,x,**cfparams)
    #title
    rpl.add_title(ax,'3.0C, {} top 300m'.format(year))
    #cbar
    rpl.add_cb(ax,cf)

  ##########ROW2 (lat-depth)
  cfparams=dict(cint0=0.1,plot_co=False)

  def mb(ax):
    ax.set_xlim(-45,75)
    ax.set_xticks([-40,-20,0, 20, 40,60])
    ax.set_xticklabels(('40S','20S','EQ', '20N', '40N', '60N'))
    ax.set_yscale('log')
    ax.set_ylim(10,5000) 
    ax.set_yticks((3000,1000,300, 100, 30,10))  
    ax.set_yticklabels((3000,1000, 300, 100, 30,10))  
    ax.invert_yaxis()
 

  #####hist
  ax=axs[2,0]   
  if year<2100:
    #var
    i=rut.find_nearest(years_his,year-5)
    x=np.mean(np.mean(x_his_atlzm[:,i:(i+11),:,:],axis=0),axis=0)-x_piclim_atlzm
    #plot
    mb(ax)
    cf=rpl.add_cf(ax,lat,lev,x,**cfparams)
    #title
    rpl.add_title(ax,'hist-rcp'.format(year))

  #####15c
  ax=axs[2,1]   
  if year>2020:
    #var
    i=rut.find_nearest(years_15c,year-5)
    x=np.mean(np.mean(x_15c_atlzm[:,i:(i+11),:,:],axis=0),axis=0)-x_piclim_atlzm
    #plot
    mb(ax)
    cf=rpl.add_cf(ax,lat,lev,x,**cfparams)
    #title
    rpl.add_title(ax,'1.5C, {}'.format(year))

  #####20c
  ax=axs[2,2]   
  if year>2040:
    #var
    i=rut.find_nearest(years_20c,year-5)
    x=np.mean(np.mean(x_20c_atlzm[:,i:(i+11),:,:],axis=0),axis=0)-x_piclim_atlzm
    #plot
    mb(ax)
    cf=rpl.add_cf(ax,lat,lev,x,**cfparams)
    #title
    rpl.add_title(ax,'2.0C, {}'.format(year))

  #####20c
  ax=axs[2,3]   
  if year>2060:
    #var
    i=rut.find_nearest(years_30c,year-5)
    x=np.mean(np.mean(x_30c_atlzm[:,i:(i+11),:,:],axis=0),axis=0)-x_piclim_atlzm
    #plot
    mb(ax)
    cf=rpl.add_cf(ax,lat,lev,x,**cfparams)
    #title
    rpl.add_title(ax,'3.0C, {}'.format(year))
    #cbar
    rpl.add_cb(ax,cf)


  ##########SAVE
  rpl.mysavefig(fig,'PLOTS/CLIM/{}_clim_{}.png'.format(var,year))
  plt.close(fig)