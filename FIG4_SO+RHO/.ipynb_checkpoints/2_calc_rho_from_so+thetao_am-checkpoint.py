#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Sat Oct  5 14:53:24 2019

@author: acrnrms
"""

import xarray as xr
import os

#Function to calculate rho0 from salinity and Temperature############### 
def calc_rho0 (s,t):
  c1p5=1.5
  rw =     9.99842594e2 + 6.793952e-2*t - 9.095290e-3*t**2\
      + 1.001685e-4*t**3 - 1.120083e-6*t**4 + 6.536332e-9*t**5

  rho0 =   rw + (8.24493e-1 - 4.0899e-3*t + 7.6438e-5*t**2\
               - 8.2467e-7*t**3 + 5.3875e-9*t**4) * s\
              + (-5.72466e-3 + 1.0227e-4*t - 1.6546e-6*t**2) * s**c1p5\
              + 4.8314e-4 * s**2
  
  return rho0


#case############### 
icase=4
if icase==1:    
    project='CanSISE'
    experimentid='historical-r1'
    yrange='195001-210012'      
    nens=5

if icase==2:    
    project='LOWWARMING'
    experimentid='lowwarming15'
    yrange='202101-260012'      
    nens=5

if icase==3:    
    project='LOWWARMING'
    experimentid='lowwarming20'
    yrange='203601-260012'      
    nens=5

if icase==4:    
    project='LOWWARMING'
    experimentid='lowwarming30'
    yrange='206101-260012'      
    nens=5

if icase==5:    
    project='CMIP5'
    experimentid='piControl'
    yrange='290101-300012'      
    nens=1



basedir='/HOME/rms/DATA/CCC/{}/{}/mon/'.format(project,experimentid)
os.system('mkdir -p {}/rho0'.format(basedir))

for i in range(1,nens+1):
  fname_s='{}/so/am_so_Omon_CanESM2_{}_r{}i1p1_{}.nc'.format(basedir,experimentid,str(i),yrange)
  fname_t='{}/thetao/am_thetao_Omon_CanESM2_{}_r{}i1p1_{}.nc'.format(basedir,experimentid,str(i),yrange)
  fname_r='{}/rho0/am_rho0_Omon_CanESM2_{}_r{}i1p1_{}.nc'.format(basedir,experimentid,str(i),yrange)

  ds_s=xr.open_dataset(fname_s,chunks={'time': 5})
  ds_t=xr.open_dataset(fname_t,chunks={'time': 5})
  ds_r=calc_rho0(ds_s.so,ds_t.thetao-273.15)
  ds_r = xr.merge([ds_r.rename('rho0'), ds_s.drop('so')]).to_netcdf(fname_r)







#rhxr=soxr
#rhxr['so'].values=soxr.so*toxr.thetao
#rhxr.to_netcdf('sotimesto_test.nc')
#rho_xr=(rho0).rename('rho').assign_attrs(so_xr.attrs)
#.to_netcdf('sotimesto_test.nc')
#soxrsel=soxr.sel(time='2021')
#sosel=soxr.sel(time='2021').so
#tosel=toxr.sel(time='2021').thetao
#soxrsel['so'].values=sosel*tosel
#soxrsel.to_netcdf('sotimesto_test.nc')






