1_calc_am.py:
SO/THETAO
lon,lat,lev,mon --> lon,lat,lev,year

2_calc_rho_from_so+thetao_am
SO+THETAO(lon,lat,lev,year) --> RHO0(lon,lat,lev,year)

3_calc-top800+plot_2Dclims.py
SO+THETAO(lon,lat,lev,year) --> SO+THETAO_top800(lon,lat,year)
                            --> SO+THETAO_top800(lat,lev,year)   
4a_calc+plot_2D_ts
SO+THETAO_top800(lon,lat,year) --> SO+THETAO_top800_atlzm(lat,year)

4b_calc+plot_so+to_ts
SO+THETAO_top800(lon,lat,year) --> SO/TO_top800_na/sa (year)

