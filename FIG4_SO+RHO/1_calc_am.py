"""
==========================
MS, create annual mean ocean data 20190927
"""
import os
import cmipdata as cd

################settings
calc_tas_pi=False
rundir='/HOME/rms/SCRIPTS/6_OTHER/6PARIS-STAB/4SO/';os.chdir(rundir)

######################################################
# 1a. Calculate GM annual mean TAS timeseries LE
######################################################
#srcdir_base='/HOME/rms/DATA/CCC/'        
srcdir_base='/raid/ra40/data/rms/DATA/CCC/'        
var='thetao'; 
explist=['lowwarming30']; project='LOWWARMING'
#explist=['historical-r1']; project='CanSISE'
#explist=['piControl']; project='CMIP5'



for exp in explist:
  indir='{}/{}/{}/mon/{}'.format(srcdir_base,project,exp,var)
  #os.system('mkdir -p {}/AM'.format(indir))
  os.chdir('{}'.format(indir))
  #os.system('ln -s {}/*r[1-5]i1*_????01-????12.nc .'.format(indir))
  ens = cd.mkensemble('*.nc')
  print 'Calculating annual mean'
  # annual means            
  my_cdo_str='cdo -setctomiss,0 -yearmean {infile} {outfile}' 
  ens_am=cd.my_operator(ens, my_cdo_str, output_prefix='am_',delete=False)
  # annual means            
  ens_am = cd.cat_exp_slices(ens_am)
