"""
==========================
MS, 20191003

Input: 
  - Data: SO/RHO_top800 (lon,lat,year) [from 3_calc-top800+plot] 
  
Output: 
  -Data&plots: SO/RHO_top800_na/sa (year) 


"""
import os
import cmipdata as cd
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt # for basic plotting
import rms_plots as rpl
import rms_utils as rut
from matplotlib.ticker import MultipleLocator, FormatStrFormatter

################settings
rundir='/HOME/rms/SCRIPTS/6_OTHER/6PARIS-STAB/4SO/';os.chdir(rundir)
mpl.style.reload_library()
plt.style.use('rms_paper')
######################################################
# 1a. Calculate GM annual mean TAS timeseries LE
######################################################
srcdir_base='/HOME/rms/DATA/CCC/'        
#srcdir_base='/raid/ra40/data/rms/DATA/CCC/'        
do_calc=False

if do_calc:
    #######Salinity##########
    os.chdir('DATA_so')
    ens_pic_top8=cd.mkensemble('top8_am_*_piControl*',prefix='top8_am_')
    ens_his_top8=cd.mkensemble('top8_am_*_historical-r*',prefix='top8_am_')
    ens_15c_top8=cd.mkensemble('top8_am_*_lowwarming15*',prefix='top8_am_')
    ens_20c_top8=cd.mkensemble('top8_am_*_lowwarming20*',prefix='top8_am_')
    ens_30c_top8=cd.mkensemble('top8_am_*_lowwarming30*',prefix='top8_am_')    

    # North Atlantic [year]
    my_cdo_str='cdo -fldmean -sellonlatbox,0,360,50,60 -ifthen /HOME/rms/DATA/MASKS/OCEAN/atlantic_mask.nc {infile} {outfile}' 
    ens_pic_top8_na_so=cd.my_operator(ens_pic_top8, my_cdo_str, output_prefix='na_',delete=False) #lon,lat,year
    ens_his_top8_na_so=cd.my_operator(ens_his_top8, my_cdo_str, output_prefix='na_',delete=False) #lon,lat,year
    ens_15c_top8_na_so=cd.my_operator(ens_15c_top8, my_cdo_str, output_prefix='na_',delete=False) #lon,lat,year
    ens_20c_top8_na_so=cd.my_operator(ens_20c_top8, my_cdo_str, output_prefix='na_',delete=False) #lon,lat,year
    ens_30c_top8_na_so=cd.my_operator(ens_30c_top8, my_cdo_str, output_prefix='na_',delete=False) #lon,lat,year
    # South Atlantic [year]
    my_cdo_str='cdo -fldmean -sellonlatbox,0,360,-30,-20 -ifthen /HOME/rms/DATA/MASKS/OCEAN/atlantic_mask.nc {infile} {outfile}' 
    ens_pic_top8_sa_so=cd.my_operator(ens_pic_top8, my_cdo_str, output_prefix='sa_',delete=False) #lon,lat,year
    ens_his_top8_sa_so=cd.my_operator(ens_his_top8, my_cdo_str, output_prefix='sa_',delete=False) #lon,lat,year
    ens_15c_top8_sa_so=cd.my_operator(ens_15c_top8, my_cdo_str, output_prefix='sa_',delete=False) #lon,lat,year
    ens_20c_top8_sa_so=cd.my_operator(ens_20c_top8, my_cdo_str, output_prefix='sa_',delete=False) #lon,lat,year
    ens_30c_top8_sa_so=cd.my_operator(ens_30c_top8, my_cdo_str, output_prefix='sa_',delete=False) #lon,lat,year

    #######rho0##########
    os.chdir('../DATA_rho0')
    # make ensemble of top 800m (lon,lat,year) timeseries [lon,lat,year] 
    ens_pic_top8=cd.mkensemble('top8_am_*_piControl*',prefix='top8_am_')
    ens_his_top8=cd.mkensemble('top8_am_*_historical-r*',prefix='top8_am_')
    ens_15c_top8=cd.mkensemble('top8_am_*_lowwarming15*',prefix='top8_am_')
    ens_20c_top8=cd.mkensemble('top8_am_*_lowwarming20*',prefix='top8_am_')
    ens_30c_top8=cd.mkensemble('top8_am_*_lowwarming30*',prefix='top8_am_')    

    # North Atlantic [year]
    my_cdo_str='cdo -fldmean -sellonlatbox,0,360,50,60 -ifthen /HOME/rms/DATA/MASKS/OCEAN/atlantic_mask.nc {infile} {outfile}' 
    ens_pic_top8_na_rho=cd.my_operator(ens_pic_top8, my_cdo_str, output_prefix='na_',delete=False) #lon,lat,year
    ens_his_top8_na_rho=cd.my_operator(ens_his_top8, my_cdo_str, output_prefix='na_',delete=False) #lon,lat,year
    ens_15c_top8_na_rho=cd.my_operator(ens_15c_top8, my_cdo_str, output_prefix='na_',delete=False) #lon,lat,year
    ens_20c_top8_na_rho=cd.my_operator(ens_20c_top8, my_cdo_str, output_prefix='na_',delete=False) #lon,lat,year
    ens_30c_top8_na_rho=cd.my_operator(ens_30c_top8, my_cdo_str, output_prefix='na_',delete=False) #lon,lat,year
    # South Atlantic [year]
    my_cdo_str='cdo -fldmean -sellonlatbox,0,360,-30,-20 -ifthen /HOME/rms/DATA/MASKS/OCEAN/atlantic_mask.nc {infile} {outfile}' 
    ens_pic_top8_sa_rho=cd.my_operator(ens_pic_top8, my_cdo_str, output_prefix='sa_',delete=False) #lon,lat,year
    ens_his_top8_sa_rho=cd.my_operator(ens_his_top8, my_cdo_str, output_prefix='sa_',delete=False) #lon,lat,year
    ens_15c_top8_sa_rho=cd.my_operator(ens_15c_top8, my_cdo_str, output_prefix='sa_',delete=False) #lon,lat,year
    ens_20c_top8_sa_rho=cd.my_operator(ens_20c_top8, my_cdo_str, output_prefix='sa_',delete=False) #lon,lat,year
    ens_30c_top8_sa_rho=cd.my_operator(ens_30c_top8, my_cdo_str, output_prefix='sa_',delete=False) #lon,lat,year


else:
    #######Salinity##########
    os.chdir('DATA_so') 

    ens_pic_top8_na_so=cd.mkensemble('na_top8_am_*_piControl*',prefix='na_top8_am_')
    ens_his_top8_na_so=cd.mkensemble('na_top8_am_*_historical-r*',prefix='na_top8_am_')
    ens_15c_top8_na_so=cd.mkensemble('na_top8_am_*_lowwarming15*',prefix='na_top8_am_')
    ens_20c_top8_na_so=cd.mkensemble('na_top8_am_*_lowwarming20*',prefix='na_top8_am_')
    ens_30c_top8_na_so=cd.mkensemble('na_top8_am_*_lowwarming30*',prefix='na_top8_am_')   

    ens_pic_top8_sa_so=cd.mkensemble('sa_top8_am_*_piControl*',prefix='sa_top8_am_')
    ens_his_top8_sa_so=cd.mkensemble('sa_top8_am_*_historical-r*',prefix='sa_top8_am_')
    ens_15c_top8_sa_so=cd.mkensemble('sa_top8_am_*_lowwarming15*',prefix='sa_top8_am_')
    ens_20c_top8_sa_so=cd.mkensemble('sa_top8_am_*_lowwarming20*',prefix='sa_top8_am_')
    ens_30c_top8_sa_so=cd.mkensemble('sa_top8_am_*_lowwarming30*',prefix='sa_top8_am_')  

    #######rho0##########
    os.chdir('../DATA_rho0') 

    ens_pic_top8_na_rho=cd.mkensemble('na_top8_am_*_piControl*',prefix='na_top8_am_')
    ens_his_top8_na_rho=cd.mkensemble('na_top8_am_*_historical-r*',prefix='na_top8_am_')
    ens_15c_top8_na_rho=cd.mkensemble('na_top8_am_*_lowwarming15*',prefix='na_top8_am_')
    ens_20c_top8_na_rho=cd.mkensemble('na_top8_am_*_lowwarming20*',prefix='na_top8_am_')
    ens_30c_top8_na_rho=cd.mkensemble('na_top8_am_*_lowwarming30*',prefix='na_top8_am_')   

    ens_pic_top8_sa_rho=cd.mkensemble('sa_top8_am_*_piControl*',prefix='sa_top8_am_')
    ens_his_top8_sa_rho=cd.mkensemble('sa_top8_am_*_historical-r*',prefix='sa_top8_am_')
    ens_15c_top8_sa_rho=cd.mkensemble('sa_top8_am_*_lowwarming15*',prefix='sa_top8_am_')
    ens_20c_top8_sa_rho=cd.mkensemble('sa_top8_am_*_lowwarming20*',prefix='sa_top8_am_')
    ens_30c_top8_sa_rho=cd.mkensemble('sa_top8_am_*_lowwarming30*',prefix='sa_top8_am_')  

os.chdir('../DATA_so') 
so_pic_na=cd.loadfiles(ens_pic_top8_na_so,'so')['data'].squeeze(); 
so_his_na=cd.loadfiles(ens_his_top8_na_so,'so')['data'].squeeze(); 
so_15c_na=cd.loadfiles(ens_15c_top8_na_so,'so')['data'].squeeze(); 
so_20c_na=cd.loadfiles(ens_20c_top8_na_so,'so')['data'].squeeze(); 
so_30c_na=cd.loadfiles(ens_30c_top8_na_so,'so')['data'].squeeze(); 

so_pic_sa=cd.loadfiles(ens_pic_top8_sa_so,'so')['data'].squeeze(); 
so_his_sa=cd.loadfiles(ens_his_top8_sa_so,'so')['data'].squeeze(); 
so_15c_sa=cd.loadfiles(ens_15c_top8_sa_so,'so')['data'].squeeze(); 
so_20c_sa=cd.loadfiles(ens_20c_top8_sa_so,'so')['data'].squeeze(); 
so_30c_sa=cd.loadfiles(ens_30c_top8_sa_so,'so')['data'].squeeze(); 


os.chdir('../DATA_rho0') 

rho_pic_na=cd.loadfiles(ens_pic_top8_na_rho,'rho0')['data'].squeeze(); 
rho_his_na=cd.loadfiles(ens_his_top8_na_rho,'rho0')['data'].squeeze(); 
rho_15c_na=cd.loadfiles(ens_15c_top8_na_rho,'rho0')['data'].squeeze(); 
rho_20c_na=cd.loadfiles(ens_20c_top8_na_rho,'rho0')['data'].squeeze(); 
rho_30c_na=cd.loadfiles(ens_30c_top8_na_rho,'rho0')['data'].squeeze(); 

rho_pic_sa=cd.loadfiles(ens_pic_top8_sa_rho,'rho0')['data'].squeeze(); 
rho_his_sa=cd.loadfiles(ens_his_top8_sa_rho,'rho0')['data'].squeeze(); 
rho_15c_sa=cd.loadfiles(ens_15c_top8_sa_rho,'rho0')['data'].squeeze(); 
rho_20c_sa=cd.loadfiles(ens_20c_top8_sa_rho,'rho0')['data'].squeeze(); 
rho_30c_sa=cd.loadfiles(ens_30c_top8_sa_rho,'rho0')['data'].squeeze(); 

os.chdir('../')



######################################################
# 2. years, picontrol climatologies
######################################################
years_his=np.arange(1950,2100+1);nyear_his=len(years_his)
years_15c=np.arange(2021,2600+1);nyear_15c=len(years_15c)
years_20c=np.arange(2036,2600+1);nyear_20c=len(years_20c)
years_30c=np.arange(2061,2600+1);nyear_30c=len(years_30c)

so_piclim_na=np.mean(so_pic_na) #constanst
so_his_na_r=so_his_na-so_piclim_na
so_15c_na_r=so_15c_na-so_piclim_na
so_20c_na_r=so_20c_na-so_piclim_na
so_30c_na_r=so_30c_na-so_piclim_na

so_piclim_sa=np.mean(so_pic_sa) #constanst
so_his_sa_r=so_his_sa-so_piclim_sa
so_15c_sa_r=so_15c_sa-so_piclim_sa
so_20c_sa_r=so_20c_sa-so_piclim_sa
so_30c_sa_r=so_30c_sa-so_piclim_sa

rho_piclim_na=np.mean(rho_pic_na) #constanst
rho_his_na_r=rho_his_na-rho_piclim_na
rho_15c_na_r=rho_15c_na-rho_piclim_na
rho_20c_na_r=rho_20c_na-rho_piclim_na
rho_30c_na_r=rho_30c_na-rho_piclim_na

rho_piclim_sa=np.mean(rho_pic_sa) #constanst
rho_his_sa_r=rho_his_sa-rho_piclim_sa
rho_15c_sa_r=rho_15c_sa-rho_piclim_sa
rho_20c_sa_r=rho_20c_sa-rho_piclim_sa
rho_30c_sa_r=rho_30c_sa-rho_piclim_sa



#####################################################################
# 3. Plot 1: 1D plots of SO/rho0 (time,lat) North and South Atlantic#
#####################################################################

#####################################################################
####### plot functions###############################################    
#####################################################################

def plot_x(ax,x_his,x_15c,x_20c,x_30c): 
  ####### plot
  ##his##
  kwargs={'linewidth': 0.5, 'color': 'gray'}
  ax.plot(years_his,np.mean(x_his,axis=0),**kwargs)                                    
  kwargs={'color':'gray', 'alpha': 0.1, 'edgecolor':'none'}
  ax.fill_between(years_his,np.min(x_his,axis=0), np.max(x_his,axis=0),**kwargs)

  ##15c##
  kwargs={'linewidth': 0.5, 'color': 'deepskyblue'}
  ax.plot(years_15c,np.mean(x_15c,axis=0),**kwargs)
  kwargs={'color':'deepskyblue', 'alpha': 0.1, 'edgecolor':'none'}
  ax.fill_between(years_15c,np.min(x_15c,axis=0), np.max(x_15c,axis=0),**kwargs)

  ##20c##
  kwargs={'linewidth': 0.5, 'color': 'orange'}
  ax.plot(years_20c,np.mean(x_20c,axis=0),**kwargs)
  kwargs={'color':'orange', 'alpha': 0.1, 'edgecolor':'none'}
  ax.fill_between(years_20c,np.min(x_20c,axis=0), np.max(x_20c,axis=0),**kwargs)

  ##30c##
  kwargs={'linewidth': 0.5, 'color': 'red'}
  ax.plot(years_30c,np.mean(x_30c,axis=0),**kwargs)
  kwargs={'color':'red', 'alpha': 0.1, 'edgecolor':'none'}
  ax.fill_between(years_30c,np.min(x_30c,axis=0), np.max(x_30c,axis=0),**kwargs)

def col1 (ax):
  ax.set_xlim([1950, 2100]) 
  ax.set_xticks(np.arange(1950,2125,25))
  ax.xaxis.set_major_locator(MultipleLocator(50))
  ax.xaxis.set_minor_locator(MultipleLocator(25)) 

def col2 (ax):
  ax.set_xlim([2100, 2600]) 
  ax.set_xticks=np.arange(2200,2700,100) 
  ax.xaxis.set_major_locator(MultipleLocator(100))
  ax.xaxis.set_minor_locator(MultipleLocator(25)) 

####### years    
years_his=np.arange(1950,2060+1)
years_15c=np.arange(2021,2600+1)
years_20c=np.arange(2036,2600+1)
years_30c=np.arange(2061,2600+1)

#####################################################################
####### Fig 1: Salinity##################################################    
#####################################################################


fig1, axs = plt.subplots(3,2, figsize=(8,11)); 
fig1.subplots_adjust(hspace=0.3,wspace=0)

so_his_na_r=so_his_na_r[:,0:rut.find_nearest(years_his,2060)+1]
so_his_sa_r=so_his_sa_r[:,0:rut.find_nearest(years_his,2060)+1]

####### a) North Atlantic    

#col1:1950-2100
ax=axs[0,0]
col1(ax)
ax.set_ylim([-0.1, 0.5])
ax.yaxis.set_major_locator(MultipleLocator(0.1))
ax.yaxis.set_minor_locator(MultipleLocator(0.02)) 
plot_x(ax, so_his_na_r, so_15c_na_r, so_20c_na_r, so_30c_na_r)

ax.set_ylabel('Salinity anomaly (PSU)')
ax.axhline(y=0.0,color="gray",linestyle='-',linewidth=1)

##legend##
ax.text(1955,0.46,'HIST+RCP8.5',color= 'gray')
ax.text(1955,0.42,'1.5$^\circ$C',color= 'deepskyblue')
ax.text(1955,0.38,'2.0$^\circ$C',color= 'orange')
ax.text(1955,0.34,'3.0$^\circ$C',color= 'red')

#col2:2100-2600
ax=axs[0,1]
col2(ax)
ax.set_ylim([-0.1, 0.5])
ax.set_yticklabels((''))
ax.yaxis.set_tick_params(length=0,width=0)
plot_x(ax, so_his_na_r, so_15c_na_r, so_20c_na_r, so_30c_na_r)
ax.axhline(y=0.0,color="gray",linestyle='-',linewidth=1)

#title
ax.text(1600,0.505,'a',fontsize=14,fontweight='bold')
ax.text(1960,0.51,'North Atlantic',fontsize=14)

####### b) South Atlantic    
#col1:1950-2100
ax=axs[1,0]
col1(ax)
ax.set_ylim([-0.1, 0.5])
ax.yaxis.set_major_locator(MultipleLocator(0.1))
ax.yaxis.set_minor_locator(MultipleLocator(0.02)) 
plot_x(ax, so_his_sa_r, so_15c_sa_r, so_20c_sa_r, so_30c_sa_r)

ax.set_ylabel('Salinity anomaly (PSU)')
ax.axhline(y=0.0,color="gray",linestyle='-',linewidth=1)


#col2:2100-2600
ax=axs[1,1]
col2(ax)
ax.set_ylim([-0.1, 0.5])
ax.set_yticklabels((''))
ax.yaxis.set_tick_params(length=0,width=0)
plot_x(ax, so_his_sa_r, so_15c_sa_r, so_20c_sa_r, so_30c_sa_r)
ax.axhline(y=0.0,color="gray",linestyle='-',linewidth=1)

#title
ax.text(1600,0.505,'b',fontsize=14,fontweight='bold')
ax.text(1960,0.51,'South Atlantic',fontsize=14)

####### c) North minus South Atlantic    
#col1:1950-2100
ax=axs[2,0]
col1(ax)
ax.set_ylim([-0.15, 0.5])
ax.yaxis.set_major_locator(MultipleLocator(0.1))
ax.yaxis.set_minor_locator(MultipleLocator(0.02)) 
plot_x(ax, so_his_na_r-so_his_sa_r, so_15c_na_r-so_15c_sa_r, so_20c_na_r-so_20c_sa_r, so_30c_na_r-so_30c_sa_r)

ax.set_ylabel('Salinity anomaly (PSU)')
ax.axhline(y=0.0,color="gray",linestyle='-',linewidth=1)


#col2:2100-2600
ax=axs[2,1]
col2(ax)
ax.set_ylim([-0.15, 0.5])
ax.set_yticklabels((''))
ax.yaxis.set_tick_params(length=0,width=0)
plot_x(ax, so_his_na_r-so_his_sa_r, so_15c_na_r-so_15c_sa_r, so_20c_na_r-so_20c_sa_r, so_30c_na_r-so_30c_sa_r)
ax.axhline(y=0.0,color="gray",linestyle='-',linewidth=1)

#title
ax.text(1600,0.505,'c',fontsize=14,fontweight='bold')
ax.text(1800,0.51,'North Atlantic minus South Atlantic',fontsize=14)


rpl.mysavefig(fig1,'PLOTS/so_atlantic_top800_ts.png')

#####################################################################
####### Fig 2: rho0##################################################    
#####################################################################


fig2, axs = plt.subplots(3,2, figsize=(8,11)); 
fig2.subplots_adjust(hspace=0.3,wspace=0)

rho_his_na_r=rho_his_na_r[:,0:rut.find_nearest(years_his,2060)+1]
rho_his_sa_r=rho_his_sa_r[:,0:rut.find_nearest(years_his,2060)+1]

####### a) North Atlantic    

#col1:1950-2100
ax=axs[0,0]
col1(ax)
ax.set_ylim([-0.4, 0.1])
ax.yaxis.set_major_locator(MultipleLocator(0.1))
ax.yaxis.set_minor_locator(MultipleLocator(0.02)) 
plot_x(ax, rho_his_na_r, rho_15c_na_r, rho_20c_na_r, rho_30c_na_r)

ax.set_ylabel('Density anomaly (kg/m$^3$)')
ax.axhline(y=0.0,color="gray",linestyle='-',linewidth=1)

##legend##
ax.text(1955,-0.27,'HIST+RCP8.5',color= 'gray')
ax.text(1955,-0.31,'1.5$^\circ$C',color= 'deepskyblue')
ax.text(1955,-0.35,'2.0$^\circ$C',color= 'orange')
ax.text(1955,-0.39,'3.0$^\circ$C',color= 'red')

#col2:2100-2600
ax=axs[0,1]
col2(ax)
ax.set_ylim([-0.4, 0.1])
ax.set_yticklabels((''))
ax.yaxis.set_tick_params(length=0,width=0)
plot_x(ax, rho_his_na_r, rho_15c_na_r, rho_20c_na_r, rho_30c_na_r)
ax.axhline(y=0.0,color="gray",linestyle='-',linewidth=1)

#title
ax.text(1600,0.105,'a',fontsize=14,fontweight='bold')
ax.text(1960,0.11,'North Atlantic',fontsize=14)

####### b) South Atlantic    

#col1:1950-2100
ax=axs[1,0]
col1(ax)
ax.set_ylim([-0.4, 0.1])
ax.yaxis.set_major_locator(MultipleLocator(0.1))
ax.yaxis.set_minor_locator(MultipleLocator(0.02)) 
plot_x(ax, rho_his_sa_r, rho_15c_sa_r, rho_20c_sa_r, rho_30c_sa_r)

ax.set_ylabel('Density anomaly (kg/m$^3$)')
ax.axhline(y=0.0,color="gray",linestyle='-',linewidth=1)

#col2:2100-2600
ax=axs[1,1]
col2(ax)
ax.set_ylim([-0.4, 0.1])
ax.set_yticklabels((''))
ax.yaxis.set_tick_params(length=0,width=0)
plot_x(ax, rho_his_sa_r, rho_15c_sa_r, rho_20c_sa_r, rho_30c_sa_r)
ax.axhline(y=0.0,color="gray",linestyle='-',linewidth=1)

#title
ax.text(1600,0.105,'b',fontsize=14,fontweight='bold')
ax.text(1960,0.11,'South Atlantic',fontsize=14)

####### c) North minus South Atlantic    

#col1:1950-2100
ax=axs[2,0]
col1(ax)
ax.set_ylim([-0.2, 0.25])
ax.yaxis.set_major_locator(MultipleLocator(0.1))
ax.yaxis.set_minor_locator(MultipleLocator(0.02)) 
plot_x(ax, rho_his_na_r-rho_his_sa_r, rho_15c_na_r-rho_15c_sa_r, rho_20c_na_r-rho_20c_sa_r, rho_30c_na_r-rho_30c_sa_r)

ax.set_ylabel('Density anomaly (kg/m$^3$)')
ax.axhline(y=0.0,color="gray",linestyle='-',linewidth=1)

#col2:2100-2600
ax=axs[2,1]
col2(ax)
ax.set_ylim([-0.2, 0.25])
ax.set_yticklabels((''))
ax.yaxis.set_tick_params(length=0,width=0)
plot_x(ax, rho_his_na_r-rho_his_sa_r, rho_15c_na_r-rho_15c_sa_r, rho_20c_na_r-rho_20c_sa_r, rho_30c_na_r-rho_30c_sa_r)
ax.axhline(y=0.0,color="gray",linestyle='-',linewidth=1)

#title
ax.text(1600,0.255,'c',fontsize=14,fontweight='bold')
ax.text(1800,0.26,'North Atlantic minus South Atlantic',fontsize=14)


rpl.mysavefig(fig2,'PLOTS/rho0_atlantic_top800_ts.png')

#####################################################################
####### Fig 3: Salinity and rho0#####################################    
#####################################################################


fig3, axs = plt.subplots(3,2, figsize=(8,11)); 
fig3.subplots_adjust(hspace=0.3,wspace=0)

so_his_na_r=so_his_na_r[:,0:rut.find_nearest(years_his,2060)+1]
so_his_sa_r=so_his_sa_r[:,0:rut.find_nearest(years_his,2060)+1]

####### a) North Atlantic    

#col1:1950-2100
ax=axs[0,0]
col1(ax)
ax.set_ylim([-0.1, 0.5])
ax.yaxis.set_major_locator(MultipleLocator(0.1))
ax.yaxis.set_minor_locator(MultipleLocator(0.02)) 
plot_x(ax, so_his_na_r, so_15c_na_r, so_20c_na_r, so_30c_na_r)

ax.set_ylabel('Salinity anomaly (PSU)')
ax.axhline(y=0.0,color="gray",linestyle='-',linewidth=1)

##legend##
ax.text(1955,0.46,'HIST+RCP8.5',color= 'gray')
ax.text(1955,0.42,'1.5$^\circ$C',color= 'deepskyblue')
ax.text(1955,0.38,'2.0$^\circ$C',color= 'orange')
ax.text(1955,0.34,'3.0$^\circ$C',color= 'red')

#col2:2100-2600
ax=axs[0,1]
col2(ax)
ax.set_ylim([-0.1, 0.5])
ax.set_yticklabels((''))
ax.yaxis.set_tick_params(length=0,width=0)
plot_x(ax, so_his_na_r, so_15c_na_r, so_20c_na_r, so_30c_na_r)
ax.axhline(y=0.0,color="gray",linestyle='-',linewidth=1)

#title
ax.text(1600,0.51,'a',fontsize=14,fontweight='bold')
ax.text(1960,0.5105,'North Atlantic Salinity',fontsize=14)

####### b) North minus South Atlantic Salinity   
#col1:1950-2100
ax=axs[1,0]
col1(ax)
ax.set_ylim([-0.15, 0.5])
ax.yaxis.set_major_locator(MultipleLocator(0.1))
ax.yaxis.set_minor_locator(MultipleLocator(0.02)) 
plot_x(ax, so_his_na_r-so_his_sa_r, so_15c_na_r-so_15c_sa_r, so_20c_na_r-so_20c_sa_r, so_30c_na_r-so_30c_sa_r)

ax.set_ylabel('Salinity anomaly (PSU)')
ax.axhline(y=0.0,color="gray",linestyle='-',linewidth=1)


#col2:2100-2600
ax=axs[1,1]
col2(ax)
ax.set_ylim([-0.15, 0.5])
ax.set_yticklabels((''))
ax.yaxis.set_tick_params(length=0,width=0)
plot_x(ax, so_his_na_r-so_his_sa_r, so_15c_na_r-so_15c_sa_r, so_20c_na_r-so_20c_sa_r, so_30c_na_r-so_30c_sa_r)
ax.axhline(y=0.0,color="gray",linestyle='-',linewidth=1)

#title
ax.text(1600,0.51,'b',fontsize=14,fontweight='bold')
ax.text(1800,0.5105,'North Atlantic minus South Atlantic Salinity',fontsize=14)

####### c) North minus South Atlantic    

#col1:1950-2100
ax=axs[2,0]
col1(ax)
ax.set_ylim([-0.15, 0.25])
ax.yaxis.set_major_locator(MultipleLocator(0.1))
ax.yaxis.set_minor_locator(MultipleLocator(0.02)) 
plot_x(ax, rho_his_na_r-rho_his_sa_r, rho_15c_na_r-rho_15c_sa_r, rho_20c_na_r-rho_20c_sa_r, rho_30c_na_r-rho_30c_sa_r)

ax.set_ylabel('Density anomaly (kg/m$^3$)')
ax.axhline(y=0.0,color="gray",linestyle='-',linewidth=1)

#col2:2100-2600
ax=axs[2,1]
col2(ax)
ax.set_ylim([-0.15, 0.25])
ax.set_yticklabels((''))
ax.yaxis.set_tick_params(length=0,width=0)
plot_x(ax, rho_his_na_r-rho_his_sa_r, rho_15c_na_r-rho_15c_sa_r, rho_20c_na_r-rho_20c_sa_r, rho_30c_na_r-rho_30c_sa_r)
ax.axhline(y=0.0,color="gray",linestyle='-',linewidth=1)

#title
ax.text(1600,0.255,'c',fontsize=14,fontweight='bold')
ax.text(1800,0.26,'North Atlantic minus South Atlantic Density',fontsize=14)
ax.text(2070,-0.25,'Year',fontsize=14)

rpl.mysavefig(fig3,'PLOTS/so+rho_atlantic_top800_ts.png')